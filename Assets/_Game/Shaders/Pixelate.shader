Shader "Hidden/Custom/Pixelate"
{
  HLSLINCLUDE
      #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
      TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
      sampler2D _CameraDepthTexture;
      
      float2 _pixels;
        
      float4 Frag(VaryingsDefault i) : SV_Target
      {
        float pixelWidth = 1.0f / _pixels.x;
		float pixelHeight = 1.0f / _pixels.y;
				
		float2 uv = half2((int)(i.texcoord.x / pixelWidth) * pixelWidth, (int)(i.texcoord.y / pixelHeight) * pixelHeight);
        return SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv);
      }
  ENDHLSL
  SubShader
  {
      Cull Off ZWrite Off ZTest Always
      Pass
      {
          HLSLPROGRAM
              #pragma vertex VertDefault
              #pragma fragment Frag
          ENDHLSL
      }
  }
}
