Shader "Hidden/Custom/DepthFog"
{
  HLSLINCLUDE
      #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
      TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
      sampler2D _CameraDepthTexture;
      
      float4 _Color;
      
      float _Density;
      
      float ComputeDistance(float depth){
        float dist = depth * _ProjectionParams.z;
        dist -= _ProjectionParams.y;
        return dist;
      }
      
      half ComputeFog(float z){
        half fog = exp2(-_Density * z);
        return saturate(fog);
      }
            
      float4 Frag(VaryingsDefault i) : SV_Target
      {
        float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord);
        float depth = tex2D(_CameraDepthTexture, i.texcoord).r;
        depth = Linear01Depth(depth);
        float dist = ComputeDistance(depth);
        half fog = 1.0 - ComputeFog(dist); 
        
        return lerp(color, _Color, fog);
      }
  ENDHLSL
  SubShader
  {
      Cull Off ZWrite Off ZTest Always
      Pass
      {
          HLSLPROGRAM
              #pragma vertex VertDefault
              #pragma fragment Frag
          ENDHLSL
      }
  }
}
