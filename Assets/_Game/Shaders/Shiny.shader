﻿Shader "Custom/Shiny"
{
    Properties
    {
        [Header(Rim)]
        [Space(7)]
        [Toggle(RIM_ENABLED)] _RimEnabled("Enabled", float) = 1
        [HDR] _RimColor("Color", Color) = (1,1,1,1)
        _RimSmoothness("Smoothness", Range(0, 1)) = 0.1
        _RimThickness("Thickness", Range(0, 1)) = 0.25
        _RimCutoff("Alpha Cutoff", Range(0, 1)) = 0.25
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            
            #pragma shader_feature RIM_ENABLED
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
            };

            uniform float _Position;
        
            float _RimSmoothness;
            float _RimThickness;
            fixed4 _RimColor;
            
            float _RimCutoff;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.normal = v.normal;
                return o;
            }
            
            float Rim(float3 normal){
                float nDotL = dot(_WorldSpaceLightPos0, normal);
                
                float p = _Position + _RimThickness;
                float n = _Position - _RimThickness;
                
                float rim = smoothstep(n - _RimSmoothness , n , nDotL)
                 - smoothstep(p, p + _RimSmoothness, nDotL);
                 
                 return rim;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                float rim = Rim(i.normal);
                clip(rim - _RimCutoff);
                return rim * _RimColor;
            }
            ENDCG
        }
    }
}