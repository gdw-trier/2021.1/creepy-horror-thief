using _Game.Scripts.Controls;
using UnityEngine;
using Cinemachine;

public class CinemachinePOVExtension : CinemachineExtension
{
    [SerializeField] private float clampAngle = 90f;
    [SerializeField] private float horizontalSpeed = 10f;
    [SerializeField] private float verticalSpeed = 10f;
    [SerializeField] private MouseSensitivity sensitivity;

    private Vector3 _startingRotation;
    private Transform headTransform;

    public bool Freeze { get; set; }

    protected override void Awake()
    {
        base.Awake();
        _startingRotation = transform.localRotation.eulerAngles;
        headTransform = GameObject.Find("Head")?.transform;
    }

    protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (vcam.Follow)
        {
            if (stage == CinemachineCore.Stage.Aim)
            {
                // don't allow camera rotation while dragging an object
                if (Freeze)
                {
                    state.RawOrientation = Quaternion.Euler(_startingRotation.y, _startingRotation.x, 0f);
                    return;
                }

                Vector2 deltaInput = InputManager.Instance.Input.Player.Look.ReadValue<Vector2>();
                _startingRotation.x += deltaInput.x * verticalSpeed * Time.deltaTime * sensitivity.sensitivity;
                _startingRotation.y -= deltaInput.y * horizontalSpeed * Time.deltaTime * sensitivity.sensitivity;
                _startingRotation.y = Mathf.Clamp(_startingRotation.y, -clampAngle, clampAngle);
                state.RawOrientation = Quaternion.Euler(_startingRotation.y, _startingRotation.x, 0f);
                if(headTransform) headTransform.localRotation = Quaternion.Euler(_startingRotation.y, _startingRotation.x, 0f);
            }
        }
    }
}
