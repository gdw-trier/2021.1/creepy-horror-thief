using System;
using UnityEngine;

public class CameraBobbing : MonoBehaviour {
    [Header("Transform references")] 
    public Transform headTransform;
    public Transform cameraTransform;

    public float bobFrequency = 3f;
    public float bobHorizontalAmplitude = 0.1f;
    public float bobVerticalAmplitude = 0.02f;
    [Range(0, 1)] public float headBobSmoothing = 0.6f;

    public bool isWalking;
    private float walkingTime;
    private Vector3 targetCameraPosition;
    
    private CharacterController _Controller;
    private CharacterController Controller => _Controller
        ? _Controller
        : (_Controller = GetComponent<CharacterController>());

    private void Update() {
        if (!isWalking) walkingTime = 0;
        else {
            walkingTime += Time.deltaTime;
            targetCameraPosition = headTransform.position + CalculateHeadBobOffset(walkingTime);

            cameraTransform.position = Vector3.Lerp(cameraTransform.position, targetCameraPosition, headBobSmoothing);

            if ((cameraTransform.position - targetCameraPosition).magnitude <= 0.001)
                cameraTransform.position = targetCameraPosition;
        }

        
    }

    private Vector3 CalculateHeadBobOffset(float t) {
        float horizontalOffset = 0;
        float verticalOffset = 0;
        Vector3 offset = Vector3.zero;

        if (t > 0) {
            horizontalOffset = (float) Math.Cos(t * bobFrequency) * bobHorizontalAmplitude;
            verticalOffset = (float) Math.Sin(t * bobFrequency * 2) * bobVerticalAmplitude;
            // forward oder right, ggf. anpassen
            offset = headTransform.forward * horizontalOffset + headTransform.up * verticalOffset;
        }
        return offset;
    }
}