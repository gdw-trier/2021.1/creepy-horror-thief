using UnityEngine;
using UnityEngine.InputSystem;

namespace _Game.Scripts.Controls {
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour {
        [SerializeField] private float playerSpeed = 2.0f;
        [SerializeField] private CinemachinePOVExtension pov;
        [SerializeField] private InventoryHandler inventory;
    
        private CharacterController _Controller;
        private CharacterController Controller => _Controller
            ? _Controller
            : (_Controller = GetComponent<CharacterController>());
        
        
        private CameraBobbing _CameraBobbing;
        private CameraBobbing CameraBobbing => _CameraBobbing
            ? _CameraBobbing
            : (_CameraBobbing = GetComponent<CameraBobbing>());
    
        
        private Camera _Camera;
        public Camera Camera => _Camera ? _Camera : (_Camera = Camera.main);

        private bool _Freeze;
        public bool Freeze {
            get => _Freeze;
            set {
                _Freeze = value;
                pov.Freeze = value;
            }
        }

        private void OnEnable() {
            InputManager.Instance.MouseLeft.OnDragStart += OnLockPerformed;
            InputManager.Instance.MouseLeft.OnDragEnd += OnLockCanceled;
            //InputManager.Instance.Input.Player.MouseUnlock.performed += OnLockPerformed;
            //InputManager.Instance.Input.Player.MouseUnlock.canceled += OnLockCanceled;
            InputManager.Instance.Input.Player.OpenInventory.performed += OnOpenInventory;
        }

        private void OnDisable() {
            InputManager.Instance.MouseLeft.OnDragStart -= OnLockPerformed;
            InputManager.Instance.MouseLeft.OnDragEnd -= OnLockCanceled;
            //InputManager.Instance.Input.Player.MouseUnlock.performed -= OnLockPerformed;
            //InputManager.Instance.Input.Player.MouseUnlock.canceled -= OnLockCanceled;
            InputManager.Instance.Input.Player.OpenInventory.performed -= OnOpenInventory;
        }

        private void OnLockPerformed() {
            Freeze = true;
        }

        private void OnLockCanceled() {
            Freeze = false;
        }
        
        private void OnOpenInventory(InputAction.CallbackContext ctx)
        {
            if (!inventory.getInventoryState())
            {
                inventory.openInventory();
            }
            else
            {
                inventory.closeInventory();
            }
        }

        void Update() {
            /*if (Freeze) {
                if (CameraBobbing) {
                    CameraBobbing.isWalking = false;
                }
                Controller.Move(Vector3.zero);
                return;
            }*/
            // Movement
            Vector2 movement = InputManager.Instance.Input.Player.Movement.ReadValue<Vector2>();
            Vector3 move = new Vector3(movement.x, 0f, movement.y);

            if (CameraBobbing) {
                if (move != Vector3.zero) {
                    CameraBobbing.isWalking = true;
                }
                else CameraBobbing.isWalking = false;
            }

            move = Camera.transform.forward * move.z + Camera.transform.right * move.x;
            move.y = 0f;
            Controller.Move(move * (Time.deltaTime * playerSpeed));
        }
    }
}