using System;
using _Game.Scripts.Utils;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace _Game.Scripts.Controls {
    public class InputManager : MonoSingleton<InputManager> {
        private PlayerControls _Input;
        public PlayerControls Input => _Input ??= new PlayerControls();

        private MouseController _MouseLeft;
        public MouseController MouseLeft => _MouseLeft ??=
            new MouseController(Mouse.current.leftButton);
        

        private void Update() {
            MouseLeft.Update();
        }

        private void OnEnable() {
            Input.Enable();
        }

        private void OnDisable() {
            Input.Disable();
        }
    }
    
    public class MouseController {
        public MouseController(ButtonControl button) {
            Button = button;
        }

        public ButtonControl Button { get; private set; }

        public DragState State { get; private set; }

        public event Action OnDragStart, OnDragEnd, OnMouseDown, OnMouseUp;
        public event Action<Vector2> OnDragUpdate;

        private Vector2 _LastPosition;
        public Vector2 Delta { get; private set; }

        public void Update() {
            switch (State) {
                case DragState.None:
                    if (!Button.wasPressedThisFrame) return;

                    _LastPosition = Mouse.current.position.ReadValue();
                    OnDragStart?.Invoke();
                    OnMouseDown?.Invoke();
                    State = DragState.Dragging;
                    return;
                case DragState.Dragging:
                    if (Button.wasReleasedThisFrame) {
                        OnMouseUp?.Invoke();
                        OnDragEnd?.Invoke();
                        State = DragState.None;
                        return;
                    }

                    Vector2 currentPosition = Mouse.current.position.ReadValue();
                    Delta = currentPosition - _LastPosition;
                    _LastPosition = currentPosition;
                    OnDragUpdate?.Invoke(Delta);
                    return;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public enum DragState {
            None,
            Dragging,
        }
    }
}