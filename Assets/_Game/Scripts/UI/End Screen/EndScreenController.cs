using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndScreenController : MonoBehaviour
{
    GameManager gameManager;
    ScoreManager scoreManager;
 
    public GameObject loseScreen;
    public GameObject winScreen;
    public Canvas highScoreCanvas;
    private CanvasVisibilityInversion highScoreScreenToggle;
    public Canvas winScreenCanvas;
    private EndScreenScoreController endScreenScoreController;
    public TMP_InputField scoreNameInput;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance;
        scoreManager = gameManager.getScoreManager();
        endScreenScoreController = winScreenCanvas.GetComponent<EndScreenScoreController>();
        Cursor.visible = true;
 
        Cursor.lockState = CursorLockMode.None;
        
        if(gameManager.PlayerisCaught)
        {
            winScreen.SetActive(false);
            loseScreen.SetActive(true);

        }
        else
        {
            loseScreen.SetActive(false);
            scoreManager.calculateScore();
            endScreenScoreController.populateText(scoreManager);
            winScreen.SetActive(true);
        }

        highScoreScreenToggle = highScoreCanvas.GetComponent<CanvasVisibilityInversion>();
    }

    public void LoadMainMenu()
    {
        if(!gameManager.PlayerisCaught)
        {
            scoreManager.commitScore();
        }
        gameManager.LoadMenu();
    }

    public void scoreNameInputEntry()
    {
        scoreManager.setScoreName(scoreNameInput.text);
    }

    /// <summary>
    /// Checks if score is high enough for leaderboard position, otherwise returns to main menu.
    /// </summary>
    public void winScreenContinueButtonCheck()
    {
        if (gameManager.checkIfHighScore(scoreManager.getScore()))
        {
            highScoreScreenToggle.openCanvas();
        }
        else
        {
            LoadMainMenu();
        }
    }
 
}
