using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndScreenScoreController : MonoBehaviour
{
    //populates Score, Highscore Placement, and loot amounts in End Screen

    public TMP_Text score;
    public TMP_Text highScoreRank;

    public TMP_Text ringA;
    public TMP_Text ringB;
    public TMP_Text ringC;
    public TMP_Text ringD;

    public TMP_Text gemA;
    public TMP_Text gemB;
    public TMP_Text gemC;
    public TMP_Text gemD;

    public TMP_Text jewelleryA;
    public TMP_Text jewelleryB;
    public TMP_Text jewelleryC;
    public TMP_Text jewelleryD;
    
    public void populateText(ScoreManager scoreManager)
    {
        score.text = scoreManager.getScore().ToString();
        highScoreRank.text = "#" + scoreManager.getScoreRank();

        ringA.text = scoreManager.getRingAAmount().ToString();
        ringB.text = scoreManager.getRingBAmount().ToString();
        ringC.text = scoreManager.getRingCAmount().ToString();
        ringD.text = scoreManager.getRingDAmount().ToString();

        gemA.text = scoreManager.getGemAAmount().ToString();
        gemB.text = scoreManager.getGemBAmount().ToString();
        gemC.text = scoreManager.getGemCAmount().ToString();
        gemD.text = scoreManager.getGemDAmount().ToString();

        jewelleryA.text = scoreManager.getJewelleryAAmount().ToString();
        jewelleryB.text = scoreManager.getJewelleryBAmount().ToString();
        jewelleryC.text = scoreManager.getJewelleryCAmount().ToString();
        jewelleryD.text = scoreManager.getJewelleryDAmount().ToString();
    }
}
