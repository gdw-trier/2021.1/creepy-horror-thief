﻿using System;
using _Game.Scripts.Systems.Interactable;
using UnityEngine;

namespace _Game.Scripts.UI {
    [RequireComponent(typeof(Collider))]
    public class PortraitHandler : MonoBehaviour, IHoverable {
        public GameObject canvas;

        private void Awake() {
            canvas.SetActive(false);
        }

        public void OnHoverStart(MouseInteraction mouse) {
            canvas.SetActive(true);
        }

        public void OnHoverEnd(MouseInteraction mouse) {
            canvas.SetActive(false);
        }
    }
}