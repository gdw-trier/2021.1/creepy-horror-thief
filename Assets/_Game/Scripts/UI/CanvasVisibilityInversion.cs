using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasVisibilityInversion : MonoBehaviour
{
    private Canvas CanvasObject;

    //Canvas gets hidden on Start()
    //public Methods to toggle visibility of Canvas Object
    void Start()
    {
        CanvasObject = GetComponent<Canvas>();
        closeCanvas(CanvasObject);
    }

    //internal toggle
    public void openCanvas()
    {
        CanvasObject.enabled = true;
    }

    public void closeCanvas()
    {
        CanvasObject.enabled = false;
    }

    //external toggle
    public void openCanvas(Canvas canvas)
    {
        canvas.enabled = true;
    }

    public void closeCanvas(Canvas canvas)
    {
        canvas.enabled = false;
    }
}
