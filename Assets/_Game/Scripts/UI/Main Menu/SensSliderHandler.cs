using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SensSliderHandler : MonoBehaviour
{
    GameManager gameManager;
    public GameObject rawInput;
    TMP_InputField rawComponent;
    public Slider slider;

    float raw;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance;
        rawComponent = rawInput.GetComponent<TMP_InputField>();
        initializeSlider();
        rawComponent.text = raw.ToString();
    }

    void initializeSlider()
    {
        raw = gameManager.getMouseSensitivity();
        slider.value = raw;
    }

    void commitSenseChanges()
    {
        gameManager.setMouseSensitivity(raw);
    }

    public float getSliderValue()
    {
        return raw;
    }

    public void sliderValChange()
    {
        raw = Mathf.Round(slider.value * 10f) * 0.1f;
        rawComponent.text = raw.ToString();
        commitSenseChanges();
    }

    public void rawInputChange()
    {
        raw = float.Parse(rawComponent.text);
        setSliderValue(raw);
    }

    public void setSliderValue(float f)
    {
        raw = Mathf.Round(f * 10f) * 0.1f;
        slider.value = raw;
        rawComponent.text = raw.ToString();
        commitSenseChanges();
    }
}
