using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class VolSliderHandler : MonoBehaviour
{
    GameManager gameManager;
    public GameObject rawInput;
    TMP_InputField rawComponent;
    public Slider slider;

    int raw; //int slider value 0-100 percent

    void Start()
    {
        gameManager = GameManager.Instance;
        initializeVolume();
        rawComponent = rawInput.GetComponent<TMP_InputField>();
        rawComponent.text = raw.ToString();
        setSliderVal(raw);
    }

    //get initial vol settings from game manager
    void initializeVolume()
    {
        if(this.tag == "MasterVolSlider")
        {
            raw = gameManager.getMasterVol();
        }
        else if(this.tag == "SFXVolSlider")
        {
            raw = gameManager.getSFXVol();
        }
        else if(this.tag == "EnemyVolSlider")
        {
            raw = gameManager.getEnemyVol();
        }
    }

    void commitVolumeChange() //commit volume changes to game manager
    {
        if (this.tag == "MasterVolSlider")
        {
            gameManager.setMasterVol(raw);
        }
        else if (this.tag == "SFXVolSlider")
        {
            gameManager.setSFXVol(raw);
        }
        else if (this.tag == "EnemyVolSlider")
        {
            gameManager.setEnemyVol(raw);
        }
    }

    /// <summary>
    /// Comits new volume values to game manager.
    /// </summary>
    public void ValueChangeCheck()
    {
        raw = Mathf.RoundToInt(slider.value * 100);
        rawComponent.text = raw.ToString();
        commitVolumeChange();
    }

    /// <summary>
    /// Takes input value, sets slider to that value and comits new volume to game manager.
    /// </summary>
    public void rawInputChange()
    {
        raw = int.Parse(rawComponent.text);
        float carry = (float)raw / 100;
        if (carry > 0)
        {
            slider.value = carry;
        }
        else if (carry > 1)
        {
            slider.value = 1f;
        }
        else
        {
            slider.value = 0.0001f;
        }

        ValueChangeCheck();


    }

    //slider value get setters for values in percent (int)
    public int getSliderVal()
    {
        return raw;
    }
    public void setSliderVal(int i)
    {
        raw = i;
        float carry = (float)i / 100;
        if(carry > 0f)
        {
            slider.value = carry;
        }
        else if(carry > 1)
        {
            slider.value = 1f;
        }
        else
        {
            slider.value = 0.0001f;
        }

        ValueChangeCheck();
    }
}
