using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCameraRotation : MonoBehaviour
{
    //bool states
    private bool firstRot;
    private bool rightRot;
    private bool runEnumerator;

    //left/right rot border + speed
    public float rightRotBorder;
    public float leftRotBorder;
    public float speed = 90f;

    //Vector3 to use the rotational floats
    private Vector3 rightBorder;
    private Vector3 leftBorder;

    // Update is called once per frame
    void Start()
    {
        firstRot = true;
        rightRot = true;
        runEnumerator = true;
        rightBorder = new Vector3(0, rightRotBorder, 0);
        leftBorder = new Vector3(0, leftRotBorder, 0);
        StartCoroutine(cameraRotation());
    }

    public void disableCameraRotation()
    {
        runEnumerator = false;
    }


    //camera rotation via Quaternions
    private IEnumerator cameraRotation()
    {
        while (runEnumerator)
        {
            var startRotation = transform.localRotation;

            if (firstRot) //rotation on initial start up
            {
                var finalRotation = Quaternion.Euler(rightBorder);
                var duration = Quaternion.Angle(startRotation, finalRotation) / speed;
                var timePassed = 0f;

                while (timePassed < duration)
                {
                    var lerpFactor = timePassed / duration;
                    var smoothedLerpFactor = Mathf.SmoothStep(0, 1, lerpFactor);

                    transform.localRotation = Quaternion.Lerp(startRotation, finalRotation, smoothedLerpFactor);

                    //timePassed += Mathf.Min(duration - timePassed, Time.deltaTime);
                }

                firstRot = false;
            }
            else if (rightRot) //right rotation
            {
                var finalRotation = Quaternion.Euler(leftBorder);
                var duration = Quaternion.Angle(startRotation, finalRotation) / speed;
                var timePassed = 0f;

                while (timePassed < duration)
                {
                    var lerpFactor = timePassed / duration;
                    var smoothedLerpFactor = Mathf.SmoothStep(0, 1, lerpFactor);

                    transform.localRotation = Quaternion.Lerp(startRotation, finalRotation, smoothedLerpFactor);

                    //timePassed += Mathf.Min(duration - timePassed, Time.deltaTime);
                }

                rightRot = false;
            }
            else //left rotation
            {
                var finalRotation = Quaternion.Euler(rightBorder);
                var duration = Quaternion.Angle(startRotation, finalRotation) / speed;
                var timePassed = 0f;

                while (timePassed < duration)
                {
                    var lerpFactor = timePassed / duration;
                    var smoothedLerpFactor = Mathf.SmoothStep(0, 1, lerpFactor);

                    transform.localRotation = Quaternion.Lerp(startRotation, finalRotation, smoothedLerpFactor);

                    //timePassed += Mathf.Min(duration - timePassed, Time.deltaTime);
                }

                rightRot = true;
            }

            yield return new WaitForSeconds(2);
        }
    }
}
