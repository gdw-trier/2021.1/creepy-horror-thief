using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreboardHandler : MonoBehaviour
{
    public TMP_Text scorePos;
    public TMP_Text scoreName;
    public TMP_Text scoreValue;
    public HighScore scores;

    //current Position determines which score gets loaded into this row.
    public int currentPosition;

    // Start is called before the first frame update
    void Start()
    {
        scorePos.text = "#"+currentPosition.ToString();
        if(currentPosition < 1)
        {
            currentPosition = 1;
        }
        scoreName.text = scores.highScoreEntriesString[currentPosition - 1];
        scoreValue.text = scores.highScoreEntriesInt[currentPosition - 1].ToString();
    }
}
