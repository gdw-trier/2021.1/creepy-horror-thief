using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using TMPro;

public class CreditsHandler : MonoBehaviour {
    public CinemachineVirtualCamera credits, menu;


    public void Open() {
        credits.Priority = 1;
        menu.Priority = 0;
    }

    public void Close() {
        credits.Priority = 0;
        menu.Priority = 1;
    }
}
