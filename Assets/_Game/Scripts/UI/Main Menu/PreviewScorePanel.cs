using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PreviewScorePanel : MonoBehaviour
{
    GameManager gameManager;
    public TMP_Text previewTopScore;
    public TMP_Text previewRecentScore;
    public HighScore scoreList;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance;
        previewTopScore.text = scoreList.highScoreEntriesInt[0].ToString();
        previewRecentScore.text = gameManager.getMostRecentScore().ToString();
    }

    
}