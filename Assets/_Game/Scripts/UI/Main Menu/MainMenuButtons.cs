using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenuButtons : MonoBehaviour
{
    GameManager gameManager;
    ScoreManager scoreManager;

    void Start()
    {
        gameManager = GameManager.Instance;
        scoreManager = gameManager.getScoreManager();
    }

    //main menu on click button functions
    public void startGame()
    {
        scoreManager.newRun();

        //loads level via Game Manager
        gameManager.LoadFirstLevel();
    }

    public void closeCredits()
    {
        gameManager.LoadMenu();
    }

    public void quitGame()
    {
        Application.Quit();
    }
}
