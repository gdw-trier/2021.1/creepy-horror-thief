﻿using _Game.Scripts.Systems.Interactable;
using UnityEngine;
using UnityEngine.InputSystem;

namespace _Game.Scripts.UI {
    [RequireComponent(typeof(Camera))]
    public class Pointer : MonoBehaviour {
        public Texture2D cursor;

        private IHoverable _Hoverable;

        private Camera _Camera;
        public Camera Camera => _Camera ? _Camera : _Camera = GetComponent<Camera>();

        private void Awake() {
            Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
        }

        private void Update() {
            var ray = Camera.ScreenPointToRay(Mouse.current.position.ReadValue());
            if (Physics.Raycast(ray, out RaycastHit hit)) {
                if (hit.collider.TryGetComponent(out IHoverable hoverable)) {
                    if (_Hoverable == hoverable) return;

                    _Hoverable?.OnHoverEnd(null);
                    _Hoverable = hoverable;
                    _Hoverable?.OnHoverStart(null);
                    return;
                }
            }

            _Hoverable?.OnHoverEnd(null);
            _Hoverable = null;
        }
    }
}