using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventoryTextPopulation : MonoBehaviour
{
    public char openInvKey;
    public TMP_Text openInvPrompt;
    public TMP_Text closeInvPrompt;

    public TMP_Text ringA;
    public TMP_Text ringB;
    public TMP_Text ringC;
    public TMP_Text ringD;

    public TMP_Text gemA;
    public TMP_Text gemB;
    public TMP_Text gemC;
    public TMP_Text gemD;

    public TMP_Text jewelleryA;
    public TMP_Text jewelleryB;
    public TMP_Text jewelleryC;
    public TMP_Text jewelleryD;

    void Start()
    {
        setNewInvKey(openInvKey);
    }

    public void populateInventory(ScoreManager scoreManager)
    {
        ringA.text = scoreManager.getRingAAmount().ToString();
        ringB.text = scoreManager.getRingBAmount().ToString();
        ringC.text = scoreManager.getRingCAmount().ToString();
        ringD.text = scoreManager.getRingDAmount().ToString();

        gemA.text = scoreManager.getGemAAmount().ToString();
        gemB.text = scoreManager.getGemBAmount().ToString();
        gemC.text = scoreManager.getGemCAmount().ToString();
        gemD.text = scoreManager.getGemDAmount().ToString();

        jewelleryA.text = scoreManager.getJewelleryAAmount().ToString();
        jewelleryB.text = scoreManager.getJewelleryBAmount().ToString();
        jewelleryC.text = scoreManager.getJewelleryCAmount().ToString();
        jewelleryD.text = scoreManager.getJewelleryDAmount().ToString();
    }

    public void setNewInvKey(char key)
    {
        openInvPrompt.text = key.ToString();
        closeInvPrompt.text = key.ToString();
    }
}
