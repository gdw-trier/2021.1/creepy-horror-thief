using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InventoryHandler : MonoBehaviour
{
    GameManager gameManager;

    public GameObject openInventoryPrompt;
    public Canvas inventory;
    Canvas inventoryCanvas;
    CanvasVisibilityInversion inventoryVisToggle;
    public InventoryTextPopulation inventoryTextPopulation;

    bool inventoryIsOpen;

    void Start()
    {
        gameManager = GameManager.Instance;
        inventoryCanvas = inventory.GetComponent<Canvas>();
        inventoryVisToggle = inventory.GetComponent<CanvasVisibilityInversion>();
        inventoryVisToggle.closeCanvas(inventoryCanvas);
        inventoryIsOpen = false;
    }

    public void openInventory()
    {
        inventoryTextPopulation.populateInventory(gameManager.getScoreManager());
        openInventoryPrompt.SetActive(false);
        inventoryVisToggle.openCanvas(inventoryCanvas);
        inventoryIsOpen = true;
    }

    public void closeInventory()
    {
        inventoryVisToggle.closeCanvas(inventoryCanvas);
        openInventoryPrompt.SetActive(true);
        inventoryIsOpen = false;
    }

    /// <summary>
    /// Returns if inventory is open or closed.
    /// </summary>
    /// <returns>Returns true if inventory is open, otherwise returns false.</returns>
    public bool getInventoryState()
    {
        return inventoryIsOpen;
    }
}
