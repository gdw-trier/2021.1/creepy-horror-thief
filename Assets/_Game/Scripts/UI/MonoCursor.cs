﻿using UnityEngine;
using UnityEngine.UI;

namespace _Game.Scripts.UI {
    public class MonoCursor : MonoBehaviour {
        public Sprite Icon { get; private set; }
        private Image _Image;
        private Sprite _DefaultIcon;
        
        private bool _Enabled = true;
        public bool Enabled {
            get => _Enabled;
            set {
                _Enabled = value;
                _Image.gameObject.SetActive(value);
            }
        }

        private void Awake() {
            _Image = GetComponentInChildren<Image>();
            _DefaultIcon = _Image.sprite;
            Debug.Log(_DefaultIcon);
        }

        public void SetIcon(Sprite icon = null) {
            if (icon == null) {
                Icon = _DefaultIcon;
                _Image.overrideSprite = _DefaultIcon;
            }

            Icon = icon;
            _Image.overrideSprite = Icon;
        }
    }
}