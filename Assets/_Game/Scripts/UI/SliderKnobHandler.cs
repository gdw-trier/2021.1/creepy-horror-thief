using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderKnobHandler : MonoBehaviour
{
    public GameObject sliderKnob;
    Transform sliderKnobTransform;
    public GameObject sliderDummy;
    Transform sliderDummyTransform;

    void Start()
    {
        sliderKnobTransform = sliderKnob.transform;
        sliderDummyTransform = sliderDummy.transform;
    }

    // Update is called once per frame
    void Update()
    {
        sliderKnobTransform.position = new Vector3(sliderDummyTransform.position.x, sliderDummyTransform.position.y, 0);
    }
}
