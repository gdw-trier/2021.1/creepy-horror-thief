using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    private bool playerisCaught;
    public GameObject ScoreManagement;
    private ScoreManager scoreManager;
    private int sceneIndex;

    private int levelIndex;

    public HighScore highScore;

    //Sound Volume variablen
    public VolumeState volumeStates;
    //Mouse sensitivity variable
    public MouseSensitivity mouseSens;
    //most recent Score
    public MostRecentScore mostRecentScore;
    public bool PlayerisCaught { get => playerisCaught; set => playerisCaught = value; }

    void Start()
    {
        instance = this;

        playerisCaught = false;

        //Load Main Menu
        SceneManager.LoadScene(1, LoadSceneMode.Additive);

        if (highScore.isNew)
            highScore.Initialize();

        if (volumeStates.isNew)
            volumeStates.Initialize();

        if (mouseSens.isNew)
            mouseSens.Initialize();

        scoreManager = ScoreManagement.GetComponent<ScoreManager>();
    }

    public static GameManager Instance {
        get {
            if (instance == null)
            {
                instance = new GameManager();
            }

            return instance;
        }
    }


    //Scene Loading
    public void LoadFirstLevel()
    {
        levelIndex = 1;
        sceneIndex = 2;
        //Unload Main Menu
        if (SceneManager.GetSceneByBuildIndex(1).isLoaded)
            SceneManager.UnloadSceneAsync(1);

        SceneManager.LoadScene(2,LoadSceneMode.Additive);
    }
    public void LoadNextLevel()
    {
        UnloadTopLevelScene();

        levelIndex++;
        sceneIndex++;
        //levelindex != sceneindex !

        if (sceneIndex == SceneManager.sceneCountInBuildSettings - 1)
        {
            sceneIndex = 2;
            SceneManager.LoadScene(sceneIndex, LoadSceneMode.Additive);
        }
        else if (sceneIndex < SceneManager.sceneCountInBuildSettings - 1)
            SceneManager.LoadScene(sceneIndex, LoadSceneMode.Additive);
        else
            LoadEndScreen(true);

        print("level: " + levelIndex);
        print("scene: " + sceneIndex);
    }


    //Main Menu Loading
    public void LoadMenu()
    {
        //Unload Game Level
        UnloadTopLevelScene();

        //Unload EndScreen
        if (SceneManager.GetSceneByName("EndScreen").isLoaded)
            SceneManager.UnloadSceneAsync("EndScreen");

        SceneManager.LoadScene(1, LoadSceneMode.Additive);

        sceneIndex = 0;
    }

    //End Screen Loading
    internal void LoadEndScreen(bool won)
    {
        PlayerisCaught = !won;

        //Unload Game Level
        UnloadTopLevelScene();

        if (won)
        {
            SceneManager.LoadScene("EndScreen", LoadSceneMode.Additive);
        }
        else
        {
            SceneManager.LoadScene("EndScreen", LoadSceneMode.Additive);
        }

        sceneIndex = SceneManager.sceneCountInBuildSettings - 1;
    }

    private int UnloadTopLevelScene()
    {

        //Top level scene index (mainscene index is 0)
        int levelIndex = SceneManager.GetSceneAt(SceneManager.sceneCount - 1).buildIndex;

        if (SceneManager.GetSceneByBuildIndex(levelIndex).isLoaded)
            SceneManager.UnloadSceneAsync(levelIndex);
        return levelIndex;
    }

    public int GetLevelNumber()
    {
        return levelIndex;
    }

    //volume control
    public int getMasterVol()
    {
        return volumeStates.masterVolume;
    }
    public float getMasterVolumeAsDecibelMod()
    {
        return volumeStates.master_dB_modifier;
    }
    public void setMasterVol(int i)
    {
        if(i < 0)
        { i = 0; }
        else if(i>100)
        { i = 100; }

        volumeStates.masterVolume = i;
        float carry = i;
        if (carry == 0f) //log cant deal with 0
            carry = 0.0001f;
        volumeStates.master_dB_modifier = Mathf.Log10(carry/100)*20;
    }
    public int getSFXVol()
    {
        return volumeStates.sfxVolume;
    }
    public float getSFXVolumeAsDecibelMod()
    {
        return volumeStates.sfx_dB_modifier;
    }
    public void setSFXVol(int i)
    {
        if (i < 0)
        { i = 0; }
        else if (i > 100)
        { i = 100; }

        volumeStates.sfxVolume = i;
        float carry = i;
        if (carry == 0f) //log cant deal with 0
            carry = 0.0001f;
        volumeStates.sfx_dB_modifier = Mathf.Log10(carry / 100) * 20;
    }
    public int getEnemyVol()
    {
        return volumeStates.enemyVolume;
    }
    public float getEnemyVolumeAsDecibelMod()
    {
        return volumeStates.enemy_dB_modifier;
    }
    public void setEnemyVol(int i)
    {
        if (i < 0)
        { i = 0; }
        else if (i > 100)
        { i = 100; }

        volumeStates.enemyVolume = i;
        float carry = i;
        if (carry == 0f) //log cant deal with 0
            carry = 0.0001f;
        volumeStates.enemy_dB_modifier = Mathf.Log10(carry / 100) * 20;
    }

    //Mouse Sensitivity Control
    public float getMouseSensitivity()
    {
        return mouseSens.sensitivity;
    }
    public void setMouseSensitivity(float sensitivity)
    {
        float carry = Mathf.Round(sensitivity * 10f) * 0.1f;
        if(carry < 0.1f)
        {
            carry = 0.1f;
        }
        else if(carry > 5f)
        {
            carry = 5f;
        }
        mouseSens.sensitivity = carry;
    }


    //score control

    /// <summary>
    /// Checks if score is high enough to make the leaderboard.
    /// </summary>
    /// <param name="newScore">Score value as integer.</param>
    /// <returns>Return true if score is high enough for leaderboard, otherwise returns false.</returns>
    public bool checkIfHighScore(int newScore)
    {
        if(newScore > highScore.highScoreEntriesInt[highScore.highScoreEntriesInt.Length -1])
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Adds new score to leaderboard. Moves all lower scores down by one. Lowest score gets discard.
    /// </summary>
    /// <param name="newScore">Score value as integer.</param>
    /// <param name="scoreName">Username as string.</param>
    public void addNewHighScore(int newScore, string scoreName)
    {
        int carryScore = 0, carryScore2 = 0;
        string carryName ="---", carryName2 = "---";
        bool gotAdded = false;
        for(int i = 0; i < highScore.highScoreEntriesInt.Length; i++)
        {
            if (!gotAdded)
            {
                if (newScore > highScore.highScoreEntriesInt[i])
                {
                    carryScore = highScore.highScoreEntriesInt[i];
                    carryName = highScore.highScoreEntriesString[i];
                    highScore.highScoreEntriesInt[i] = newScore;
                    highScore.highScoreEntriesString[i] = scoreName;
                    gotAdded = true;
                }
            }
            else
            {
                if(carryScore > highScore.highScoreEntriesInt[i])
                {
                    carryScore2 = highScore.highScoreEntriesInt[i];
                    carryName2 = highScore.highScoreEntriesString[i];
                    highScore.highScoreEntriesInt[i] = carryScore;
                    highScore.highScoreEntriesString[i] = carryName;
                    carryScore = carryScore2;
                    carryName = carryName2;
                }
            }
        }
    }

    /// <summary>
    /// Gets possible leaderboard position for score.
    /// </summary>
    /// <param name="newScore">Score value as integer.</param>
    /// <returns>Returns 0 if score is too low for leaderboard, otherwise returns leaderboard position.</returns>
    public int getNewHighScoreIndex(int newScore)
    {
        int index = 0;
        for(int i = 0; i < highScore.highScoreEntriesInt.Length; i++)
        {
            if(newScore > highScore.highScoreEntriesInt[i])
            {
                index = i + 1;
                break;
            }
        }
        return index;
    }
    public int getMostRecentScore()
    {
        return mostRecentScore.GetScore();
    }
    public void setMostRecentScore(int newScore)
    {
        mostRecentScore.score = newScore;
    }

    public ScoreManager getScoreManager()
    {
        return scoreManager;
    }
}
