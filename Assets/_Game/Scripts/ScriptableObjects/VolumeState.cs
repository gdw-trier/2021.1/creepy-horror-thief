using System;
using UnityEngine;

[CreateAssetMenu(fileName = "VolumeState", menuName = "ScriptableObjects/VolumeState", order = 2)]
public class VolumeState : ScriptableObject
{
    //0-100
    public int masterVolume;
    public int sfxVolume;
    public int enemyVolume;

    //0 if vol = 100; -120 if vol = 0;
    public float master_dB_modifier;
    public float sfx_dB_modifier;
    public float enemy_dB_modifier;

    public bool isNew = true;

    internal void Initialize()
    {
        masterVolume = 100;
        sfxVolume = 100;
        enemyVolume = 100;
        master_dB_modifier = 0;
        sfx_dB_modifier = 0;
        enemy_dB_modifier = 0;
        isNew = false;
    }
}
