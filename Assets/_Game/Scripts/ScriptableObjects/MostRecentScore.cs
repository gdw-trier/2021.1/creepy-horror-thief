using UnityEngine;

[CreateAssetMenu(fileName = "MostRecentScore", menuName = "ScriptableObjects/MostRecentScore", order = 3)]
public class MostRecentScore : ScriptableObject
{
    public int score;

    public int GetScore()
    {    
        return score;
    }
}
