using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="MouseSensitivity", menuName = "ScriptableObjects/MouseSensitivity", order = 4)]
public class MouseSensitivity : ScriptableObject
{
    public float sensitivity = 1f;
    public bool isNew;

    internal void Initialize()
    {
        sensitivity = 1;
        isNew = false;
    }
}
