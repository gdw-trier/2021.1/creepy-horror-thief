using System;
using UnityEngine;

[CreateAssetMenu(fileName = "HighScore", menuName = "ScriptableObjects/HighScore", order = 1)]
public class HighScore : ScriptableObject
{
    public string Title;

    public int[] highScoreEntriesInt = new int[10];
    public string[] highScoreEntriesString = new string[10];
    public bool isNew = true;
    
    internal void Initialize()
    {
        for (int i = 0; i < highScoreEntriesInt.Length; i++)
        {
            highScoreEntriesInt[i] = 0;
            highScoreEntriesString[i] = "---";
        }

        isNew = false;
    }
}

