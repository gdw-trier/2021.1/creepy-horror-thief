using UnityEngine;

public class LordSoundController : MonoBehaviour
{
    public AudioSource vocalSRC;
    public AudioSource stepSRC;
    public AudioClip laughter;
    private void Start()
    {
        VolumeState vlmState = GameManager.Instance.volumeStates;
        float volume = .7f;
        /*
        if (vlmState?.enemyVolume != null)
        {
            volume = (float)(vlmState.enemyVolume / 100);
            volume *= (float)(vlmState.masterVolume / 100);
        }
        */

        stepSRC.volume = volume;
        stepSRC.pitch = .3f;
        vocalSRC.volume = 0.1f;
        vocalSRC.pitch = 2.0f;
    }

    public void PlayEvilLaughter()
    {
        vocalSRC.PlayOneShot(laughter);
    }
}
