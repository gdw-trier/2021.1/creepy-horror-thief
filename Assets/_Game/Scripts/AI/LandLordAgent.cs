using UnityEngine.AI;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Cinemachine;

public class LandLordAgent : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    [SerializeField] private LandLordState state;
    private List<Vector3> _patrolPoints;
    private int patrolPointIndex;
    private Transform playerTransform;
    [SerializeField] private Vector3 _dest = new Vector3(1, 0, 2);
    private Vector3 lastKnownPlayerPos;
    private Vector3 cameraForward;
    private Vector3 cameraToLord;

    public LordSoundController soundController;
    public CinemachineVirtualCamera landLordCam;
    public AgentController agentController;
    public float _agentSpeed = 1f;
    public float killDistance = 2f;
    public Animator lordAC;
    public float detectionRange = 8;

    public LandLordState State { get => state; set => state = value; }

    public enum LandLordState
    {
        Idle, Searching, GTG, Killing
    }

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        State = LandLordState.Idle;
        navMeshAgent.stoppingDistance = 1.5f;
        navMeshAgent.acceleration = 60;
        navMeshAgent.angularSpeed = 360f;
        navMeshAgent.height = 1.8f;
        navMeshAgent.autoRepath = true;
        navMeshAgent.autoBraking = false;
        navMeshAgent.radius = .25f;

        //Animation speed relative to speed of navmeshagent
        lordAC.speed = _agentSpeed * 1.5f;

     

        playerTransform = agentController.playerTransform;


        landLordCam.enabled = false;
        playerTransform.gameObject.GetComponentInChildren<CinemachineVirtualCamera>().enabled = true;
        playerTransform = agentController.playerTransform;
        landLordCam.Priority = 0;
        playerTransform.gameObject.GetComponentInChildren<CinemachineVirtualCamera>().Priority = 100;


        if (agentController.patrolPoints.Count > 0)
        {
            _patrolPoints = agentController.patrolPoints;
            patrolPointIndex = 0;
            _dest = _patrolPoints[patrolPointIndex];
        }

        SetDestination(_dest);
        cameraForward = Camera.main.transform.forward;
        cameraToLord = new Vector3();
    }

    void Update()
    {
        //setting the movementspeed value in the animator:
        lordAC.SetFloat("MovementSpeed", navMeshAgent.speed);

        float dist = 1000;

        //state machine

        switch (State)
        {
            case LandLordState.Idle:
                break;
            case LandLordState.Killing:

                //cam lerp to lord


                Camera.main.transform.forward = Vector3.Lerp(cameraForward, cameraToLord, 1.0f * Time.deltaTime);

                break;
            case LandLordState.Searching:

                if (!navMeshAgent.isStopped)
                    dist = CalculateDistance(navMeshAgent.transform.position, navMeshAgent.destination);

                if (dist < navMeshAgent.stoppingDistance)
                {
                    StopAgent();
                    //Small pause at destination
                    StartCoroutine(WaitAtDestinationAndGTG(3));
                }

                if (CalculateDistance(navMeshAgent.transform.position, playerTransform.position) < killDistance)
                {
                    //is the landlord able to see the player
                    if (!navMeshAgent.Raycast(playerTransform.position, out NavMeshHit hit)) { 
                        StopAgent();

                        cameraForward = Camera.main.transform.forward;
                        cameraToLord = Vector3.Normalize(this.transform.position - Camera.main.transform.position);
                        State = LandLordState.Killing;


                 
                        playerTransform.gameObject.GetComponentInChildren<CinemachineVirtualCamera>().enabled = false;
                        landLordCam.enabled = true;
               


                        //Game over 
                        print("you lose");
                        //death animation
                        StartCoroutine(SlowMotionWaitAndGameOver(2));

                    }
                }
                DetectPlayer();

                break;

            case LandLordState.GTG:

                //if landlord is able to see player after idle time, he is following him again
                if(!DetectPlayer()) 
                { 
                    if (!navMeshAgent.isStopped)
                        dist = CalculateDistance(navMeshAgent.transform.position, navMeshAgent.destination);

                    //TODO: find cheaper solution
                    if (dist > navMeshAgent.stoppingDistance)
                    {
                        SetDestination(agentController.GetBossSpawnPosition());
                    }
                    else
                    {
                        //Despawn
                        StopAgent();
                        this.gameObject.SetActive(false);
                        agentController.IsLandLordSpawned = false;
                    }
                }
                break;

            default:
                break;
        }

    }

 

    private bool DetectPlayer()
    {
        Vector3 lookingDirection = this.gameObject.transform.forward;
        Vector3 playerPostition = playerTransform.position;
        Vector3 lineAgentToPlayer = playerPostition - this.transform.position;

        float dotProduct = Vector3.Dot(lineAgentToPlayer, lookingDirection);

        //If the dot product between the line from boss agent to player and its looking direction ist higher than zero 
        //the agent is facing roughly towards the player and...
        //If distance to player is smaller than detection range, cast ray to player
        if (dotProduct > 0 && lineAgentToPlayer.magnitude < detectionRange)
        {
            //bool isHit = navMeshAgent.Raycast(playerPostition, out NavMeshHit hit);
            RaycastHit hit;
            bool isHit = false;

            if (Physics.Raycast(transform.position, lineAgentToPlayer, out hit, Mathf.Infinity))
            {
                if (hit.collider.name.Equals("Player"))
                {
                    isHit = true;
                    print("agent: player detected");
                }

            }

            if (isHit)
            {
                lastKnownPlayerPos = playerPostition;
 
                State = LandLordState.Searching;
                SetDestination(lastKnownPlayerPos);
                return true;
            }
        }

        return false;
    }
    private IEnumerator WaitAtDestinationAndGTG(int seconds)
    {
        yield return new WaitForSeconds(seconds);

        SetDestination(agentController.GetBossSpawnPosition());

        State = LandLordState.GTG;
    }
    private IEnumerator SlowMotionWaitAndGameOver(int seconds)
    {
        lordAC.SetTrigger("KillTrigger");
        soundController.PlayEvilLaughter();
        Time.timeScale = .3f;
        yield return new WaitForSeconds(seconds);
        Time.timeScale = 1.0f;
        agentController.GameOver();

        playerTransform.gameObject.GetComponentInChildren<CinemachineVirtualCamera>().enabled = true;
        landLordCam.enabled = false;
        
    }

    public void SetDestination(Vector3 dest)
    {

        navMeshAgent.destination = dest;
        navMeshAgent.speed = _agentSpeed;
        navMeshAgent.isStopped = false;
    }
 
    private void StopAgent()
    {
        navMeshAgent.isStopped = true;
        navMeshAgent.speed = 0f;
    }

    private float CalculateDistance(Vector3 pos, Vector3 dest)
    {
        pos = new Vector3(pos.x, 0, pos.z);
        dest = new Vector3(dest.x, 0, dest.z);

        return Vector3.Distance(pos, dest);
    }
    private void FindNextTarget()
    {

        _patrolPoints = agentController.patrolPoints;

        ++patrolPointIndex;

        //Normierung auf Listenlnge
        patrolPointIndex %= _patrolPoints.Count;

        _dest = _patrolPoints[patrolPointIndex];

        SetDestination(_dest);
    }

}
