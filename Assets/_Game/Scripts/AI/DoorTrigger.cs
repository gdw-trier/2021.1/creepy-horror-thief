using _Game.Scripts.Systems.Interactable;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Door")
        {
            //Open Door

            other.gameObject.GetComponent<DoorDraggable>().Open();

        }
    }

    private void OnTriggerExit(Collider other)
    { 
        //other.gameObject.name.Equals("Door")
        if (other.gameObject.tag == "Door")
        {
            //Close Door
            other.gameObject.GetComponent<DoorDraggable>().Close();
        }
    }

}
