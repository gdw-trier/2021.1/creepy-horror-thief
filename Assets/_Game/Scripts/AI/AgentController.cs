using UnityEngine.AI;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class AgentController : MonoBehaviour
{
    private Agent[] agents;
    private bool isLandLordSpawned;
    private Vector3 _lastKnownPlayerPos;
    private GameManager gameManager;
    private Transform[] patrolPointsTransforms;

    public List<Vector3> patrolPoints;
    public int soundAlertRange = 5;
    public GameObject agentPool;
    public GameObject pois;
    public bool isSoundAlert;
    public Transform soundAlertPosition;
    public Transform playerTransform;
    public LandLordAgent landLord;

    public GameObject aiAgentPrefab;
    public Vector3 LastKnownPlayerPos { get => _lastKnownPlayerPos; set => _lastKnownPlayerPos = value; }
    public bool IsLandLordSpawned { get => isLandLordSpawned; set => isLandLordSpawned = value; }

    void Awake()
    {
        gameManager = GameManager.Instance;

        isSoundAlert = false;
        IsLandLordSpawned = false;
        landLord.gameObject.SetActive(false);
        //landLord.landLordCam.enabled = false;
        playerTransform.gameObject.GetComponentInChildren<CinemachineVirtualCamera>().enabled = true;

        patrolPointsTransforms = pois.GetComponentsInChildren<Transform>();

        for (int i = 0; i < patrolPointsTransforms.Length; i++)
        {
            patrolPoints.Add(patrolPointsTransforms[i].position);
        }

        //Spawn amount: 
        int agentAmount = 1;
        int levelNumber = gameManager.GetLevelNumber();
        //int agentAmount = (int) (gameManager.GetLevelNumber() * 1.5f);
        /*
         * Level 1: 1 goblin
         * Ab Level 3: 2 Goblin
         * Ab Level 12: 3 Goblin
         * Ab Level 30: 4 Goblin
         */
        if (levelNumber > 2)
            agentAmount = 2;
        if (levelNumber > 11)
            agentAmount = 3;
        if (levelNumber > 29)
            agentAmount = 4;
        SpawnAgents(agentAmount);

        agents = agentPool.GetComponentsInChildren<Agent>();
    }

    private void SpawnAgents(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject agentInstance = GameObject.Instantiate(aiAgentPrefab);
            agentInstance.transform.SetParent(agentPool.transform);
            Agent agent = agentInstance.GetComponent<Agent>();
            agent.SetAgentController(this);

            print("navmeshagent, nr: " + i +" - "+ agent.GetComponent<NavMeshAgent>());

            if (i < patrolPointsTransforms.Length)
                agent.GetComponent<NavMeshAgent>().Warp(patrolPointsTransforms[i].position);
            else if (patrolPointsTransforms.Length > 0)
                agent.GetComponent<NavMeshAgent>().Warp(patrolPointsTransforms[0].position);
            else
                agent.GetComponent<NavMeshAgent>().Warp(new Vector3(0,0,0));

        }
    }

    private void Update()
    {
        if (isSoundAlert) { 
            SoundEventPropagation(soundAlertPosition.position);
            isSoundAlert = false;
        }
    }

    public void SoundEventPropagation(Vector3 soundEventPos)
    {
        for (int i = 0; i < agents.Length; i++)
        {
            Vector3 agentPos = agents[i].GetPosition();
            float distance = Vector3.Distance(soundEventPos, agentPos);

            if(distance < soundAlertRange)
            {
                //alert agent
                agents[i].AgentState = Agent.State.SearchingSound;
                agents[i].SetDestination(soundEventPos);
            }
        }
    }

    internal void AlertPlayerPositonToAll(Vector3 lastKnownPlayerPos)
    {
            _lastKnownPlayerPos = lastKnownPlayerPos;  
    }

    public Vector3 GetBossSpawnPosition()
    {
        Vector3 playerPos = playerTransform.position;

        float longestDistance = 0;
        int index = 0;
        for (int i = 0; i < patrolPoints.Count; i++)
        {
            float distance = Vector3.Distance(playerPos, patrolPoints[i]);

            if (distance > longestDistance)
            { 
                longestDistance = distance;
                index = i;
            }
        }
        return patrolPoints[index];
    }

    internal void GameOver()
    {
        StopAllCoroutines();
        gameManager.LoadEndScreen(false);
    }

    internal void CallForLandLord(Vector3 lastKnownPlayerPos)
    {
        if(!IsLandLordSpawned)
        {
            //Spawn landlord
            IsLandLordSpawned = true;
            landLord.transform.position = GetBossSpawnPosition();
            
            landLord.gameObject.SetActive(true);
        }
        landLord.SetDestination(lastKnownPlayerPos);
        landLord.State = LandLordAgent.LandLordState.Searching;
    }
}
