using System;
using System.Collections.Generic;
using UnityEngine;

public class AgentSoundController : MonoBehaviour
{

    public AudioSource stepSRC;
    public AudioSource vocalSRC;
    public List<AudioClip> audioClips;
    public float stepSpeed;

    private void Start()
    {
        VolumeState vlmState = GameManager.Instance.volumeStates;
        float volume = .7f;
        /*
        if (vlmState?.enemyVolume != null) { 
             volume = (float)(vlmState.enemyVolume / 100);
             volume *= (float)(vlmState.masterVolume / 100);
        }       
        */
        stepSRC.volume = volume;
        vocalSRC.volume = volume;
        
        stepSRC.pitch = UnityEngine.Random.Range(1f, 2f);
        PlaySteps(true);

    }

    internal void PlayLaughter()
    {
        if (!vocalSRC.isPlaying) {
            if (audioClips.Count > 0)
            {
                vocalSRC.PlayOneShot(audioClips[0]);
                vocalSRC.pitch = UnityEngine.Random.Range(1f, 2f);
            }
        }

    }

    internal void PlaySteps(bool playSteps)
    {

        if (playSteps && !stepSRC.isPlaying)
            StartPlayingSteps();
        else if(!playSteps)
            stepSRC.Stop();
    }


    private void StartPlayingSteps()
    {

        if (audioClips.Count > 0)
        {
            stepSRC.clip = audioClips[1];
            stepSRC.Play();
        }
    }
}
