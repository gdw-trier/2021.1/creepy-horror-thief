using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;


public class Agent : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    [SerializeField] private State agentState = State.Idle;
    private List<Vector3> _patrolPoints;
    private int patrolPointIndex;
    private Vector3 lastKnownPlayerPos;
    private Transform playerTransform;
    [SerializeField] private Vector3 _dest;
    private bool isPointing;

    //public Transform lastKnownPlayerPosTransform;
    public Transform soundEventPositionTransform;
    public bool agentHasSeenPlayer;
    public int detectionRange = 3;
    public int actionRange = 2;
    public Animator agentAC;
    public float _agentSpeed = 1f;  
    public AgentController agentController;
    public AgentSoundController soundController;
    //Properties
    public State AgentState { get => agentState; set => agentState = value; }
    public NavMeshAgent NavMeshAgent { get => navMeshAgent; set => navMeshAgent = value; }

    public enum State 
    {
        Idle, Patrolling, SearchingSound, AlertCharging, AlertWaiting, Pointing, Hysterical
    }

    void Start()
    {
        _dest = new Vector3(1, 0, 2);
        isPointing = false;
        playerTransform = agentController.playerTransform;


        NavMeshAgent = GetComponent<NavMeshAgent>();

        NavMeshAgent.stoppingDistance = 2f;
        NavMeshAgent.acceleration = 60;
        NavMeshAgent.angularSpeed = 360f;
        NavMeshAgent.height = 1.8f;
        NavMeshAgent.autoRepath = true;
        NavMeshAgent.autoBraking = false;
        NavMeshAgent.radius = .25f;

        agentHasSeenPlayer = false;
        AgentState = State.Patrolling;
 
        //Animation speed relative to speed of navmeshagent
        agentAC.speed = _agentSpeed * 1.5f + 0.5f;

        if (agentController.patrolPoints.Count > 0)
        {
            _patrolPoints = agentController.patrolPoints;
            FindNextTargetRandom();
        }
    }

    internal Vector3 GetPosition()
    {
        return transform.position;
    }

    void Update()
    {
        //setting the movementspeed value in the animator:
        agentAC.SetFloat("MovementSpeed", NavMeshAgent.speed);

        soundController.PlaySteps(NavMeshAgent.speed > 0.2f);
        //soundController.stepSpeed = navMeshAgent.speed * 0.5f;

        float dist = 1000;

        //State machine
        switch (AgentState)
        {
            case State.Idle:
                DetectPlayer();

                break;

            case State.Pointing:
                soundController.PlayLaughter();

                break;

            case State.Hysterical:


                break;

            case State.Patrolling:
                //To avoid flickering, distance is just calculated while moving           
                if (!NavMeshAgent.isStopped)
                    dist = CalculateDistance(NavMeshAgent.transform.position, NavMeshAgent.destination);

                if (dist < actionRange)
                {
                    StopAgent();
                    //Small pause at destination
                    StartCoroutine(WaitAtDestinationAndPatrolToNextTarget(1));
                }
                else
                    DetectPlayer();
                break;

            case State.AlertCharging:

                //Play laughing sound if not already playing

                dist = CalculateDistance(NavMeshAgent.transform.position, NavMeshAgent.destination);

                if (dist < actionRange)
                {
                    AgentState = State.Hysterical;

                    StopAgent();
                    //waiting at destination
                    StartCoroutine(WaitAtDestinationAndHype(2));

                }
                break;

            case State.AlertWaiting:
           
                //start patrolling again after a pause
                if(agentHasSeenPlayer)
                    StartCoroutine(WaitAtDestinationAndPatrolToNextTarget(5));
                //reset player detection

                break;

            case State.SearchingSound:
                //if sound is played near agent, its position is the agents destination
                if (!NavMeshAgent.isStopped)
                    dist = CalculateDistance(NavMeshAgent.transform.position, NavMeshAgent.destination);

                if (dist < actionRange)
                {
                    StopAgent();
                    //waiting at destination
                    StartCoroutine(WaitAtDestinationAndPatrolToNextTarget(4));
                    //agentController.AlertPlayerPositonToAll(lastKnownPlayerPos);
                }

                DetectPlayer();

                break;

            default:
                break;
        }



    }

    private IEnumerator WaitAtDestinationAndPoint(float v)
    {
        StopAgent();

        Vector3 lineAgentToPlayer = playerTransform.position - this.transform.position;

        this.transform.forward = Vector3.Normalize(lineAgentToPlayer);

        agentAC.SetBool("isPointing", true);
        yield return new WaitForSeconds(v);
        agentAC.SetBool("isPointing", false);

        AgentState = State.AlertCharging;
        SetDestination(lastKnownPlayerPos);

    }
    private IEnumerator WaitAtDestinationAndHype(float v)
    {

        agentAC.SetBool("isHysterical", true);
        agentAC.SetBool("isPointing", false);
        yield return new WaitForSeconds(v);
        agentAC.SetBool("isHysterical", false);
        AgentState = State.AlertWaiting;

    }
    private void DetectPlayer()
    {
        Vector3 lookingDirection = this.gameObject.transform.forward;
        Vector3 playerPostition = playerTransform.position;
        Vector3 lineAgentToPlayer = playerPostition - this.transform.position;

        float dotProduct = Vector3.Dot(lineAgentToPlayer, lookingDirection);

        //If the agent hasen't already detected the player and...
        //If the dot product between the line from agent to player and its looking direction ist higher than zero 
        //the agent is facing roughly towards the player and...
        //If distance to player is smaller than detection range, cast ray to player
        if (!agentHasSeenPlayer && dotProduct > 0 && lineAgentToPlayer.magnitude < detectionRange)
        {
             bool isHit = NavMeshAgent.Raycast(playerPostition, out NavMeshHit hit);
            /*
            RaycastHit hit;
            bool isHit = false;

            if (Physics.Raycast(transform.position, lineAgentToPlayer, out hit, Mathf.Infinity))
            {
                if (hit.collider.name.Equals("Player"))
                {
                    isHit = true;
                    print("agent: player detected");
                }

            }
            */


            if (!isHit)
            {
                agentHasSeenPlayer = true;
                //Set last known player position to lure boss to
                lastKnownPlayerPos = playerPostition;
                //lastKnownPlayerPosTransform.position = lastKnownPlayerPos;

                AgentState = State.Pointing;
                StartCoroutine(WaitAtDestinationAndPoint(1.5f));

                agentController.CallForLandLord(lastKnownPlayerPos);
            }
        }
    }

    IEnumerator WaitAtDestinationAndPatrolToNextTarget(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        agentHasSeenPlayer = false;
        AgentState = State.Patrolling;

        FindNextTargetRandom();
    }

    private float CalculateDistance(Vector3 pos, Vector3 dest)
    {
        pos = new Vector3(pos.x,0, pos.z);
        dest = new Vector3(dest.x, 0, dest.z);

        return Vector3.Distance(pos, dest);
    }

    private void FindNextTarget()
    {
        if (_patrolPoints.Count > 0) { 
            _patrolPoints = agentController.patrolPoints;

            ++patrolPointIndex;

            //Normierung auf Listenlšnge
            patrolPointIndex %= _patrolPoints.Count;

            _dest = _patrolPoints[patrolPointIndex];

            SetDestination(_dest);
        }
    }

    private void FindNextTargetRandom()
    {
        if (_patrolPoints.Count > 0)
        {

            int randomIndex = UnityEngine.Random.Range(0, _patrolPoints.Count);

            _dest = _patrolPoints[randomIndex];

            SetDestination(_dest);
        }
    }

    public void SetDestination(Vector3 dest)
    {
        NavMeshAgent.destination = dest;
        NavMeshAgent.speed = _agentSpeed;
        NavMeshAgent.isStopped = false;
    }

    private void StopAgent()
    {
        NavMeshAgent.isStopped = true;
        NavMeshAgent.speed = 0f;
    }

    public void SetAgentController(AgentController agentC)
    {
        agentController = agentC;
    }
}
