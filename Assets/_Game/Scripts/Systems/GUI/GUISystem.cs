﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Systems.GUI {
    public class GUISystem : MonoBehaviour {
         //Stack of screens
        private readonly Stack<GUIScreen> _ScreenStack = new Stack<GUIScreen>(2);
        
        //All screens that are part of this menu
        private GUIScreen[] _Screens = new GUIScreen[0];
        
        [Tooltip("Screen that will be shown on start")]
        public GUIScreen entry;
        
        [Tooltip("On start which active status should the screen have")]
        public bool active = true;
        
        public UnityEvent onSwitchedScreens, onEnterSystem, onExitSystem;
        
        /// <summary>
        /// The current used screen
        /// </summary>
        public GUIScreen CurrentScreen => _ScreenStack.Peek();

        public GUIScreen this[int index] => _Screens[index];
        
        private void Awake(){
            _Screens = GetComponentsInChildren<GUIScreen>(true);
            Entry(entry, active);
        }
        
        /// <summary>
        /// Set entry screen
        /// </summary>
        /// <param name="screen">Screen to start with</param>
        /// <param name="init">Active status on start</param>
        private void Entry(GUIScreen screen, bool init){
            if(!screen) return;

            foreach (var uiScreen in _Screens){
                if (uiScreen != screen){
                    uiScreen.gameObject.SetActive(false);
                    continue;
                }
                
                _ScreenStack.Push(uiScreen);
                CurrentScreen.OnEnter(this);
                CurrentScreen.gameObject.SetActive(init);
            }
        }
        
        /// <summary>
        /// Switch screen
        /// </summary>
        /// <param name="screen">Screen to switch too</param>
        public void SwitchScreens(GUIScreen screen){
            if (!screen) return;
            
            CurrentScreen.OnExit(this);
            _ScreenStack.Pop();
            _ScreenStack.Push(screen);
            CurrentScreen.OnEnter(this);
            CurrentScreen.gameObject.SetActive(true);
            
            onSwitchedScreens?.Invoke();
        }
        
        /// <summary>
        /// Push screen on stack. Needed if screen is a sub screen.
        /// </summary>
        /// <param name="screen">Screen to switch to</param>
        public void PushScreen(GUIScreen screen){
            if (!screen) return;
            
            CurrentScreen.OnExit(this);
            _ScreenStack.Push(screen);
            CurrentScreen.OnEnter(this);
            CurrentScreen.gameObject.SetActive(true);
            
            onSwitchedScreens?.Invoke();
        }
        
        /// <summary>
        /// Pop screen from stack. Needed if you want to go back from a sub screen
        /// </summary>
        public void PopScreen(){
            if (_ScreenStack.Count <= 1) return;
            
            CurrentScreen.OnExit(this);
            _ScreenStack.Pop();
            CurrentScreen.OnEnter(this);
            CurrentScreen.gameObject.SetActive(true);
            
            onSwitchedScreens?.Invoke();
        }
        
        /// <summary>
        /// On System Enter
        /// </summary>
        /// <param name="manager">parent UIManager</param>
        public virtual void OnEnter(GUIManager manager){
            onEnterSystem?.Invoke();
            gameObject.SetActive(true);
        }
        
        /// <summary>
        /// On System Exit
        /// </summary>
        /// <param name="manager">parent UIManager</param>
        public virtual void OnExit(GUIManager manager){
            onExitSystem?.Invoke();
            gameObject.SetActive(false);
        }

    }
}