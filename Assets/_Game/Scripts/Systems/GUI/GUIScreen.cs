﻿using Cinemachine;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Systems.GUI {
    /// <summary>
    /// Represents a UI page
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public class GUIScreen : MonoBehaviour {

        public new CinemachineVirtualCamera camera;
        public UnityEvent onScreenEnter, onScreenExit;

        private void Awake(){
            //Set entry selectable
            //if (entry) EventSystem.current.SetSelectedGameObject(entry.gameObject);
        }
        
        /// <summary>
        /// On Screen Enter
        /// </summary>
        /// <param name="system">The UISystem witch this screen is parented to</param>
        public virtual void OnEnter(GUISystem system){
            onScreenEnter?.Invoke();
            gameObject.SetActive(true);

            if (camera) camera.Priority = 10;
        }
        
        /// <summary>
        /// On Screen Exit
        /// </summary>
        /// <param name="system">The UISystem witch this screen is parented to</param>
        public virtual void OnExit(GUISystem system){
            onScreenExit?.Invoke();
            gameObject.SetActive(false);

            if (camera) camera.Priority = 0;
        }
    }

}