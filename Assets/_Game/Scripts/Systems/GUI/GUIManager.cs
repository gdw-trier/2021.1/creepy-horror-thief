﻿using _Game.Scripts.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Systems.GUI {
    /// <summary>
    /// UIManager is managing all menus (UISystem) that are part of it. In Detail it controls which menu is shown and allows switching between.
    /// </summary>
    public class GUIManager : MonoSingleton<GUIManager>{
        //All found system
        private GUISystem[] _Systems = new GUISystem[0];
        
        //System to start with
        [Tooltip("System to start with")]
        public GUISystem entry;
        
        //GameObject active status at start
        [Tooltip("What active status should the entry system have")]
        public bool active = true;
        
        //Every time called system is switched
        public UnityEvent onSwitchedSystem;
        
        //Get the current used System
        public GUISystem CurrentSystem{ get; private set; }

        /// <summary>
        /// Enable or disable current use system
        /// </summary>
        /// <param name="value"></param>
        public void EnableSystem(bool value){
            CurrentSystem.gameObject.SetActive(value);
        }
        
        /// <summary>
        /// Get managed system with index
        /// </summary>
        /// <param name="index">Find index by looking which position the system is as a child object to the object UIManager is applied to</param>
        public GUISystem this[int index]{
            get => _Systems[index];
        }

        protected virtual void Awake(){
            _Systems = GetComponentsInChildren<GUISystem>(true);
            Entry(entry, active);
        }
        
        /// <summary>
        /// Set entry point
        /// </summary>
        /// <param name="system">System to start with</param>
        /// <param name="init">on start what active status should the system have</param>
        private void Entry(GUISystem system, bool init){
            if (!system) return;
            
            foreach (var uiSystem in _Systems){
                if (uiSystem != system){
                    uiSystem.gameObject.SetActive(false);
                    continue;
                }

                CurrentSystem = uiSystem;
                CurrentSystem.OnEnter(this);
                CurrentSystem.gameObject.SetActive(init);
            }
        }
        
        /// <summary>
        /// Switch systems
        /// </summary>
        /// <param name="system">System to switch to</param>
        public void SwitchSystems(GUISystem system){
            if (!system) return;

            CurrentSystem.OnExit(this);
            CurrentSystem = system;
            CurrentSystem.OnEnter(this);
            CurrentSystem.gameObject.SetActive(true);

            onSwitchedSystem?.Invoke();
        }
    }

}