﻿using System;
using _Game.Scripts.Systems.Sound;
using _Game.Scripts.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Game.Scripts.Systems.Ambient {
    [CreateAssetMenu(fileName = "Thunder_Storm_Settings", menuName = "Avaritia/Thunder Storm Settings")]
    public class ThunderStormSettings : ScriptableObject {
        public float duration = 2f;
        
        [Header("Lightning")]
        public float lightningIntensity = 60f;
        public Color lightningColor = Color.white;
        public AnimationCurve lightningCurve;

        [Header("Glass Emission")] public Material glass;
        [ColorUsage(false, true)] public Color emissionColor = Color.white;
        public AnimationCurve emissionCurve;
        private Timer _Animation = new Timer();
        private static readonly int _EmissionColor = Shader.PropertyToID("_EmissionColor");
        
        [Header("Sounds")] 
        public AudioClip[] sounds;
        private Timer _SoundTimer = new Timer();

        public bool Running => _SoundTimer.Running;

        public event Action OnEnd;

        public void Trigger(MonoBehaviour behaviour) {
            if (Running) return;

            _SoundTimer.OnEnd = null;
            _SoundTimer.OnEnd += Finished;
            var clip = sounds[Random.Range(0, sounds.Length)];
            _SoundTimer.Run(behaviour, clip.length);
            GlobalAudioSource.Instance.OverlaySource.ScheduleClip(clip);
            
            glass.EnableKeyword("_EMISSION");
            var current = glass.GetColor(_EmissionColor);
            _Animation.Run(behaviour, duration, t => {
                glass.SetColor(_EmissionColor,
                    Color.Lerp(emissionColor, current, emissionCurve.Evaluate(t)));
            });
            
            Lightning.Trigger(
                duration,
                lightningIntensity,
                lightningColor,
                lightningCurve
            );
        }

        private void Finished() {
            OnEnd?.Invoke();
        }
    }
}