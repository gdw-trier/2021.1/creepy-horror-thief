using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace _Game.PostProcessing {
    [Serializable]
    [PostProcess(typeof(FogRenderer), PostProcessEvent.BeforeStack, "Custom/DepthFog")]
    public class Fog : PostProcessEffectSettings {
        public ColorParameter color = new ColorParameter();
        [Range(0f, 1f)]public FloatParameter density = new FloatParameter();
    }

    public class FogRenderer : PostProcessEffectRenderer<Fog> {
        private static readonly int _Color = Shader.PropertyToID("_Color");
        private static readonly int _Density = Shader.PropertyToID("_Density");

        public override void Render(PostProcessRenderContext context) {
            var sheet = context.propertySheets.Get(Shader.Find("Hidden/Custom/DepthFog"));
            sheet.properties.SetColor(_Color, settings.color);
            sheet.properties.SetFloat(_Density, settings.density);
            context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
        }
    }
}