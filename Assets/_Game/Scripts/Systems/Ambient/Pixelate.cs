using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace _Game.Scripts.Systems.Ambient {
    [Serializable]
    [PostProcess(typeof(PixelateRenderer), PostProcessEvent.AfterStack, "Custom/Pixelate")]
    public class Pixelate : PostProcessEffectSettings
    {
        public IntParameter width = new IntParameter() {value = Screen.width};
        public IntParameter height = new IntParameter(){value = Screen.height};
    }

    public class PixelateRenderer : PostProcessEffectRenderer<Pixelate> {
        private static readonly int _Pixels = Shader.PropertyToID("_pixels");


        public override void Render(PostProcessRenderContext context) {
            var sheet = context.propertySheets.Get(Shader.Find("Hidden/Custom/Pixelate"));
            sheet.properties.SetVector(_Pixels, new Vector2(settings.width, settings.height));
            context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
        }
    }
}
