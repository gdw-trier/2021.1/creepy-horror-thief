using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using _Game.Scripts.Utils;
using UnityEngine;

namespace _Game.Scripts.Systems.Ambient {
    [RequireComponent(typeof(Light))]
    public class Lightning : MonoBehaviour {
        public bool overwrite = false;
        public float duration = 4f;
        public float maxIntensity;
        public Color color;
        public AnimationCurve curve;

        private Light _light;
        private Light Light => _light ? _light : (_light = GetComponent<Light>());

        private static Dictionary<GameObject, Lightning> _sources;

        private static Dictionary<GameObject, Lightning> Sources =>
            _sources ??= new Dictionary<GameObject, Lightning>();

        private Timer _Animation = new Timer();

        private void OnEnable() {
            Sources.Add(gameObject, this);
        }

        private void OnDisable() {
            Sources.Remove(gameObject);
        }


        public static void Trigger(float duration, float maxIntensity, Color color,
            AnimationCurve curve) {
            foreach (var source in Sources) {
                var value = source.Value;
                float minIntensity = value.Light.intensity;
                Color minColor = value.Light.color;
                if (value.overwrite) {
                    value._Animation.Run(value, value.duration, t => {
                        value.Light.intensity = Mathf.Lerp(value.maxIntensity, minIntensity, value.curve.Evaluate(t));
                        value.Light.color = Color.Lerp(value.color, minColor, value.curve.Evaluate(t));
                    });
                    return;
                }
                
                value._Animation.Run(value, duration, t => {
                    value.Light.intensity = Mathf.Lerp(maxIntensity, minIntensity, curve.Evaluate(t));
                    value.Light.color = Color.Lerp(color, minColor, curve.Evaluate(t));
                });
            }
        }
    }
}