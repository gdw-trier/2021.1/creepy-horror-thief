using _Game.Scripts.Utils;
using _Game.Scripts.Utils.Editor;
using UnityEngine;

namespace _Game.Scripts.Systems.Ambient {
    public class ThunderStorm : MonoSingleton<ThunderStorm> {
        [Header("Settings")]
        public ThunderStormSettings stormSettings;

        [Header("Timings")] public float delay = 5f;
        [MinMaxSlider(-10f, 10f)]public Vector2 offsetRange = new Vector2(-2f, 2f);
        
        private Timer _Timer = new Timer();

        private void Awake() {
            Run();
        }

        private void OnEnable() {
            if (stormSettings) stormSettings.OnEnd += Run;
        }

        private void OnDisable() {
            if (stormSettings) stormSettings.OnEnd -= Run;
        }

        private void Run() {
            _Timer.Run(this, delay + Random.Range(offsetRange.x, offsetRange.y));
            _Timer.OnEnd = null;
            _Timer.OnEnd += Trigger;
        }

        [ContextMenu("Trigger")]
        public void Trigger() {
            if(!stormSettings) return;
            stormSettings.Trigger(this);
        }
    }
}