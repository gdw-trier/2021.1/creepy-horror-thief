using System.Collections.Generic;
using UnityEngine;

namespace _Game.Scripts.Ambient {
    [RequireComponent(typeof(MeshRenderer))]
    public class ShinyEffect : MonoBehaviour {
        public float speed = 1f;
        [ColorUsage(true, true)] public Color color = Color.white;
        [Range(0f, 1f)] public float smoothness = 0.1f;
        [Range(0f, 1f)] public float thickness = 0.1f;

        private float _value;

        private MeshRenderer _renderer;

        private MeshRenderer Renderer =>
            _renderer ? _renderer : (_renderer = GetComponent<MeshRenderer>());

        private MaterialPropertyBlock _property;
        private MaterialPropertyBlock PropertyBlock => _property ??= new MaterialPropertyBlock();

        private static readonly int _Position = Shader.PropertyToID("_Position");
        private static readonly int _RimThickness = Shader.PropertyToID("_RimThickness");
        private static readonly int _RimSmoothness = Shader.PropertyToID("_RimSmoothness");
        private static readonly int _RimColor = Shader.PropertyToID("_RimColor");

        private void Reset() {
            Shader shader = Shader.Find("Custom/Shiny");
            if (shader) {
                List<Material> materials = new List<Material>(Renderer.sharedMaterials);
                var shinyMat = new Material(shader);
                materials.Add(shinyMat);
                Renderer.sharedMaterials = materials.ToArray();
            }
        }

        private void OnValidate() {
            Renderer.GetPropertyBlock(PropertyBlock);
            PropertyBlock.SetColor(_RimColor, color);
            PropertyBlock.SetFloat(_RimSmoothness, smoothness);
            PropertyBlock.SetFloat(_RimThickness, thickness);
            Renderer.SetPropertyBlock(PropertyBlock);
        }

        private void LateUpdate() {
            UpdateEffect();
        }

        private void UpdateEffect() {
            if (Renderer) {
                Renderer.GetPropertyBlock(PropertyBlock);
                _value = _value >= 2.0f ? -2.0f : _value + speed * Time.deltaTime;
                PropertyBlock.SetFloat(_Position, _value);
                Renderer.SetPropertyBlock(PropertyBlock);
            }
        }
    }
}