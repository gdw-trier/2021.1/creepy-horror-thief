using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LootSpawnPoint : MonoBehaviour {
    [SerializeField] private GameObject[] prefabsRing;
    [SerializeField] private GameObject[] prefabsGem;
    [SerializeField] private GameObject[] prefabsJewellery;
    
    //[SerializeField] private GameObject[] lootGameObjects;
    [SerializeField] private int spawnChance = 50;

    private static float[] percentageFloor1 = {
        42.9f, 7.5f, 3f, 1f,        // Rings A - D
        29f, 5f, 2.5f, 0.5f,        // Gems A - D
        12.5f, 4f, 1f, 0.1f         // Jewellery A - D
    };
    private static float[] percentageFloor2To68 = {
        33.7f, 8.5f, 3.1f, 1.01f,        // Rings A - D
        26f, 5.7f, 2.55f, 0.501f,        // Gems A - D
        13.5f, 4.3f, 1.03f, 0.1001f         // Jewellery A - D
    };
    private static float[] percentageFloor69To99 = {
        0.1f, 3f, 18.5f, 7.5f,        // Rings A - D
        0.5f, 5f, 30.4f, 5f,        // Gems A - D
        1f, 7.5f, 18.5f, 3f         // Jewellery A - D
    };
    private static float[] percentageFloor100 = {
        0.1f, 1f, 4f, 12.5f,        // Rings A - D
        0.5f, 2.5f, 5f, 20f,        // Gems A - D
        1f, 3f, 7.5f, 42.9f         // Jewellery A - D
    };

    private static String[] lootRarity = {
        "A", "B", "C", "D",         // Rings A - D
        "A", "B", "C", "D",         // Gems A - D
        "A", "B", "C", "D"           // Jewellery A - D
    };

    private static Dictionary<GameObject, LootSpawnPoint> _spawnPoints;

    private static Dictionary<GameObject, LootSpawnPoint> SpawnPoints =>
        _spawnPoints ??= new Dictionary<GameObject, LootSpawnPoint>();

    private void OnEnable() {
        SpawnPoints.Add(gameObject, this);
    }

    private void OnDisable() {
        SpawnPoints.Remove(gameObject);
    }

    public static void SpawnAll(int floor) {
        float[] percentages;
        if (floor == 1) percentages = percentageFloor1;
        else if (floor < 69) percentages = percentageFloor2To68;
        else if (floor < 100) percentages = percentageFloor69To99;
        else percentages = percentageFloor100;

        int counter = 0;
        foreach (var spawnPoint in SpawnPoints) {
            if(spawnPoint.Value.Spawn(percentages)) {
                counter += 1;
            }
        }
        Debug.Log("Spawned " + counter + " of " + SpawnPoints.Count + " elements");
    }

    private bool Spawn(float[] percentages) {
        // random chance to spawn an object at this spawn point
        int chance = Random.Range(0,100);
        if (chance < spawnChance) {
            
            // use percentages to define which object spawns depending on the floor level 
            GameObject[][] lootPrefabs = {
                prefabsRing, prefabsRing, prefabsRing, prefabsRing,
                prefabsGem, prefabsGem, prefabsGem, prefabsGem,
                prefabsJewellery, prefabsJewellery, prefabsJewellery, prefabsJewellery
            };

            int index = GetRandomWeightedIndex(percentages);
            GameObject[] prefabOfType = lootPrefabs[index];

            // randomly select a prefab of the specific type
            int prefabIndex = Random.Range(0, prefabOfType.Length);
            GameObject loot = Instantiate(prefabOfType[prefabIndex], transform);
            loot.GetComponent<LootType>().rarity = lootRarity[index];
            return true;
        }
        return false;
    }
    
    // https://forum.unity.com/threads/random-numbers-with-a-weighted-chance.442190/
    public int GetRandomWeightedIndex(float[] weights)
    {
        // Get the total sum of all the weights.
        float weightSum = 0f;
        for (int i = 0; i < weights.Length; ++i)
        {
            weightSum += weights[i];
        }
 
        // Step through all the possibilities, one by one, checking to see if each one is selected.
        int index = 0;
        int lastIndex = weights.Length - 1;
        while (index < lastIndex)
        {
            // Do a probability check with a likelihood of weights[index] / weightSum.
            if (Random.Range(0, weightSum) < weights[index])
            {
                return index;
            }
 
            // Remove the last item from the sum of total untested weights and try again.
            weightSum -= weights[index++];
        }
 
        // No other item was selected, so return very last index.
        return index;
    }
}