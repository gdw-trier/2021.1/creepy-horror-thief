using TMPro;
using UnityEngine;

public class LevelSign : MonoBehaviour
{
    public TextMeshPro text;

    void Start()
    {
        int nextLevel = GameManager.Instance.GetLevelNumber() + 1;
        text.SetText("Level " + nextLevel);
    }
}
