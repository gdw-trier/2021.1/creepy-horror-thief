using UnityEngine;

public class ExitController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name.Equals("Player"))
        {
            //GameManager.Instance.LoadEndScreen(true);
            GameManager.Instance.LoadNextLevel();
        }
    }
}
