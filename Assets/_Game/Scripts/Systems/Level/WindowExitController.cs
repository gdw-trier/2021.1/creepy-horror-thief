using UnityEngine;


namespace _Game.Scripts.Systems.Interactable {
public class WindowExitController : MonoBehaviour, IInteractable
{
 
    public void Click()
    {
        GameManager.Instance.LoadEndScreen(true);
    }

}
}