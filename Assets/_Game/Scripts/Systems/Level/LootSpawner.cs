using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootSpawner : MonoBehaviour
{
    void Start()
    {
        LootSpawnPoint.SpawnAll(GameManager.Instance.GetLevelNumber());
    }

}
