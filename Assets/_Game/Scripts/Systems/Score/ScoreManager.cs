using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    GameManager gameManager;
    private int currentScore;
    private string scoreName;
    private int scoreRank;

    private int itemQuantity;
    private int[,] itemList;
    /*
     * 1st dimenion:
     *      0: Ring
     *      1: Gems
     *      2: Jewelry
     * 2nd dimension:
     *      0: Type A
     *      1: Type B
     *      2: Type C
     *      3: Type D
     */

    private void Start()
    {
        gameManager = GameManager.Instance;
        initializeRun();
    }

    private void initializeRun()
    {
        currentScore = 0;
        scoreRank = 0;
        itemQuantity = 0;
        itemList = new int[3, 4];
        for (int i = 0; i < itemList.GetLength(0); i++)
        {
            for(int j = 0; j < itemList.GetLength(1); j++)
            {
                itemList[i,j] = 0;
            }
        }
    }

    /// <summary>
    /// Resets itemlist, score, and floor for a new run.
    /// </summary>
    public void newRun()
    {
        initializeRun();
    }

    /// <summary>
    /// Commits score to game manager as most recent score. Adds score to highscore leaderboard if applicable.
    /// </summary>
    public void commitScore()
    {
        if(gameManager.checkIfHighScore(currentScore))
        {
            gameManager.addNewHighScore(currentScore, scoreName);
        }
        gameManager.setMostRecentScore(currentScore);
    }

    /// <summary>
    /// Iterates over item list to get score value.
    /// </summary>
    public void calculateScore()
    {
        for(int i = 0; i < itemList.GetLength(0); i++)
        {
            for(int j = 0; j < itemList.GetLength(1); j++)
            {
                if(i == 0)
                {
                    if(j == 0)
                    {
                        currentScore += itemList[0,0] * 100;
                    }
                    else if(j == 1)
                    {
                        currentScore += itemList[0,1] * 1000;
                    }
                    else if(j == 2)
                    {
                        currentScore += itemList[0,2] * 5000;
                    }
                    else
                    {
                        currentScore += itemList[0,3] * 10000;
                    }
                }
                else if(i == 1)
                {
                    if (j == 0)
                    {
                        currentScore += itemList[1,0] * 500;
                    }
                    else if (j == 1)
                    {
                        currentScore += itemList[1,1] * 5000;
                    }
                    else if (j == 2)
                    {
                        currentScore += itemList[1,2] * 25000;
                    }
                    else
                    {
                        currentScore += itemList[1,3] * 50000;
                    }
                }
                else if(i == 2)
                {
                    if (j == 0)
                    {
                        currentScore += itemList[2,0] * 1000;
                    }
                    else if (j == 1)
                    {
                        currentScore += itemList[2,1] * 10000;
                    }
                    else if (j == 2)
                    {
                        currentScore += itemList[2,2] * 50000;
                    }
                    else
                    {
                        currentScore += itemList[2,3] * 100000;
                    }
                }
            }
        }
    }

    public int getScore()
    {
        return currentScore;
    }
    public void setScoreName(string name)
    {
        scoreName = name;
    }
    /// <summary>
    /// Gets highscore leaderboard position for current score.
    /// </summary>
    /// <returns>Returns 0 if score too low to make the leaderboard, otherwise returns leaderboard position.</returns>
    public int getScoreRank()
    {
        if (gameManager.checkIfHighScore(currentScore))
        {
            scoreRank = gameManager.getNewHighScoreIndex(currentScore);
        }
        return scoreRank;
    }
    
    /// <summary>
    /// Adds loot to itemlist based on LootType component.
    /// </summary>
    /// <param name="lootType">.type: 'Ring', 'Gem', 'Jewellery'; .rarity: 'A', 'B', 'C', 'D'</param>
    public void lootPickUp(LootType lootType) 
    {
        itemQuantity++;

        switch(lootType.type)
        {
            case "Ring":
                if(lootType.rarity == "A")
                { itemList[0, 0]++; }
                else if(lootType.rarity == "B")
                { itemList[0, 1]++; }
                else if(lootType.rarity == "C")
                { itemList[0, 2]++; }
                else
                { itemList[0, 3]++; }
                break;
            case "Gem":
                if (lootType.rarity == "A")
                { itemList[1, 0]++; }
                else if (lootType.rarity == "B")
                { itemList[1, 1]++; }
                else if (lootType.rarity == "C")
                { itemList[1, 2]++; }
                else
                { itemList[1, 3]++; }
                break;
            case "Jewellery":
                if (lootType.rarity == "A")
                { itemList[2, 0]++; }
                else if (lootType.rarity == "B")
                { itemList[2, 1]++; }
                else if (lootType.rarity == "C")
                { itemList[2, 2]++; }
                else
                { itemList[2, 3]++; }
                break;
        }
    }


    public int[,] getItemList()
    {
        return itemList;
    }

    /// <summary>
    /// Returns amount of items currently stored in the itemlist.
    /// </summary>
    /// <returns>Returns item quantity as integer.</returns>
    public int getTotalItemQuantity()
    {
        return itemQuantity;
    }

    //loot amount getters
    public int getRingAAmount()
    {
        return itemList[0, 0];
    }
    public int getRingBAmount()
    {
        return itemList[0, 1];
    }
    public int getRingCAmount()
    {
        return itemList[0, 2];
    }
    public int getRingDAmount()
    {
        return itemList[0, 3];
    }
    public int getGemAAmount()
    {
        return itemList[1, 0];
    }
    public int getGemBAmount()
    {
        return itemList[1, 1];
    }
    public int getGemCAmount()
    {
        return itemList[1, 2];
    }
    public int getGemDAmount()
    {
        return itemList[1, 3];
    }
    public int getJewelleryAAmount()
    {
        return itemList[2, 0];
    }
    public int getJewelleryBAmount()
    {
        return itemList[2, 1];
    }
    public int getJewelleryCAmount()
    {
        return itemList[2, 2];
    }
    public int getJewelleryDAmount()
    {
        return itemList[2, 3];
    }
}
