﻿using System;
using _Game.Scripts.Controls;
using _Game.Scripts.UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace _Game.Scripts.Systems.Interactable {
    [RequireComponent(typeof(Camera))]
    public class MouseInteraction : MonoBehaviour {
        public float distance = 2f;
        public LayerMask mask;

        public MonoCursor crosshair;
        public Sprite interactIcon;

        private Camera _Camera;
        private Camera Camera => _Camera ? _Camera : (_Camera = GetComponent<Camera>());
        private IDraggable _Current;

        public bool IsHovering => _Hoverable != null;
        private IHoverable _Hoverable;

        private void Start() {
            //Cursor.SetCursor(defaultIcon, Vector2.zero, CursorMode.Auto);
            Cursor.lockState = CursorLockMode.Locked;
            //Cursor.visible = true;
        }

        private void OnEnable() {
            InputManager.Instance.MouseLeft.OnDragStart += StartDrag;
            InputManager.Instance.MouseLeft.OnDragUpdate += UpdateDrag;
            InputManager.Instance.MouseLeft.OnDragEnd += EndDrag;
        }

        private void OnDisable() {
            InputManager.Instance.MouseLeft.OnDragStart -= StartDrag;
            InputManager.Instance.MouseLeft.OnDragUpdate -= UpdateDrag;
            InputManager.Instance.MouseLeft.OnDragEnd -= EndDrag;
        }

        private void Update() {
            if (_Current != null) return;
            var ray = Camera.ScreenPointToRay(Mouse.current.position.ReadValue());
            if (Physics.Raycast(ray, out RaycastHit hit)) {
                if (hit.collider.TryGetComponent(out IHoverable hoverable)) {
                    if (_Hoverable == hoverable) return;

                    _Hoverable?.OnHoverEnd(this);
                    if (crosshair) crosshair.SetIcon(interactIcon);
                    _Hoverable = hoverable;
                    _Hoverable?.OnHoverStart(this);
                    return;
                }
            }

            if (crosshair && _Hoverable != null) crosshair.SetIcon();
            _Hoverable?.OnHoverEnd(this);
            _Hoverable = null;
        }

        private void StartDrag() {
            crosshair.Enabled = false;
            var ray = Camera.ScreenPointToRay(Mouse.current.position.ReadValue());
            if (Physics.Raycast(ray, out RaycastHit hit, distance, mask)) {
                if (hit.collider.TryGetComponent(out IInteractable collectable)) {
                    collectable.Click();
                    Debug.Log($"Collected {collectable}");
                    return;
                }

                if (hit.collider.TryGetComponent(out _Current)) {
                    _Current.DragStart();
                    Cursor.lockState = CursorLockMode.Confined;
                    Cursor.visible = false;
                    Debug.Log($"Dragging {_Current}");
                    return;
                }

                return;
            }

            Debug.Log("Did not hit anything!");
            _Current = null;
        }

        private void UpdateDrag(Vector2 delta) {
            if (_Current is MonoBehaviour mono) {
                if ((transform.position - mono.transform.position).sqrMagnitude >
                    distance * distance) {
                    EndDrag();
                    return;
                }
            }

            _Current?.DragUpdate(delta);
        }

        private void EndDrag() {
            Cursor.lockState = CursorLockMode.Locked;
            if (crosshair) crosshair.Enabled = true;
            _Current?.DragEnd();
            _Current = null;
        }
    }
}
