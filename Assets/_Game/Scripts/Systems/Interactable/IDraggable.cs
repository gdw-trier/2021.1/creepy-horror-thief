﻿using UnityEngine;

namespace _Game.Scripts.Systems.Interactable {
    /// <summary>
    /// IDraggable
    /// </summary>
    public interface IDraggable {
        /// <summary>
        /// Will be called on drag start
        /// </summary>
        /// <param name="delta">Mouse delta at the start of drag</param>
        public void DragStart();
        /// <summary>
        /// Will be called while mouse is dragging (On every frame)
        /// </summary>
        /// <param name="delta">Mouse delta while dragging</param>
        public void DragUpdate(Vector2 delta);
        /// <summary>
        /// Will be called on drag end
        /// </summary>
        /// <param name="delta">Mouse delta at the end of drag</param>
        public void DragEnd();
    }
}