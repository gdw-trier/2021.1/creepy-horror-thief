﻿using _Game.Scripts.Utils;
using UnityEngine;

namespace _Game.Scripts.Systems.Interactable {
    [RequireComponent(typeof(Rigidbody), typeof(HingeJoint))]
    public class DoorDraggable : MonoBehaviour, IDraggable, IHoverable {
        public float strength = 1.0f;
        public bool maintainMomentum = true;
        public float damping = 5.0f;
        [Range(0f, 1f)] public float threshold = 0.95f;
        public Transform orientation;

        public bool IsOpen => Vector3.Dot(_Tangent.normalized, transform.forward) < threshold;

        //References
        private Rigidbody _Rigidbody;
        private HingeJoint _Joint;

        private Camera _Camera;
        private Camera Camera => _Camera ? _Camera : (_Camera = Camera.main);

        private Transform Coord => orientation == null ? transform : orientation;
        private Vector3 _Origin, _Normal, _Tangent, _Bitangent;
        private Vector3 _Axis;

        private Timer _AnimationTimer = new Timer();

        private void Awake() {
            _Rigidbody = GetComponent<Rigidbody>();
            _Joint = GetComponent<HingeJoint>();

            _Rigidbody.drag = damping;

            _Origin = Coord.position;
            float d = 1f;
            _Normal = Coord.TransformDirection(Vector3.up).normalized * d;
            _Tangent = Coord.TransformDirection(Vector3.forward).normalized * d;
            _Bitangent = Coord.TransformDirection(Vector3.right).normalized * d;
        }


        [ContextMenu("Open")]
        public void Open() {
            if (!IsOpen) {
                Rotate(_Joint.limits.max);
            }
        }

        [ContextMenu("Close")]
        public void Close() {
            if (IsOpen) {
                Rotate(-Vector3.SignedAngle(_Tangent, transform.forward, _Joint.axis));
            }
        }

        private void Rotate(float angle) {
            var current = transform.rotation;
            var target = Quaternion.FromToRotation(_Tangent,
                Quaternion.AngleAxis(angle, _Joint.axis) * _Tangent) * current;
            _AnimationTimer.Run(this, 1f,
                t => {
                    _Rigidbody.rotation = Quaternion.Lerp(current, target,
                        AnimationCurve.EaseInOut(0, 0, 1, 1).Evaluate(t));
                });
        }

        public void DragStart() {
            if (!maintainMomentum) {
                _Rigidbody.velocity = Vector3.zero;
                return;
            }

            _Axis = (Camera.WorldToScreenPoint(_Origin + _Tangent) -
                     Camera.WorldToScreenPoint(_Origin)).normalized;
        }

        public void DragUpdate(Vector2 delta) {
            Vector3 force = _Joint.axis * (-Vector2.Dot(_Axis, delta) *
                                           strength * Time.deltaTime);
            _Rigidbody.AddTorque(force);
        }

        public void DragEnd() {
            if (maintainMomentum) return;

            _Rigidbody.velocity = Vector3.zero;
        }

        public void OnHoverStart(MouseInteraction mouse) {
        }

        public void OnHoverEnd(MouseInteraction mouse) {
        }

        private void OnDrawGizmos() {
            DrawLine(_Origin, _Origin + _Normal, Color.green);
            DrawLine(_Origin, _Origin + _Tangent, Color.blue);
            DrawLine(_Origin, _Origin + _Bitangent, Color.red);
        }

        private void DrawLine(Vector3 start, Vector3 end, Color color) {
            Gizmos.color = color;
            Gizmos.DrawLine(start, end);
            Gizmos.color = Color.white;
        }
    }
}