﻿namespace _Game.Scripts.Systems.Interactable {
    public interface IHoverable {
        public void OnHoverStart(MouseInteraction mouse);
        public void OnHoverEnd(MouseInteraction mouse);
    }
}