using System;
using _Game.Scripts.Utils;
using UnityEngine;

namespace _Game.Scripts.Systems.Interactable {
    public class Collectable : MonoBehaviour, IInteractable, IHoverable
    {
        private ScoreManager _scoreManager;
        public LootType lootType;
        private AudioSource _audioSource;

        private void Start()
        {
            _scoreManager = GameManager.Instance.getScoreManager();
            lootType = GetComponent<LootType>();
            _audioSource ??= GetComponent<AudioSource>();
        }

        public void Click()
        {
            _scoreManager.lootPickUp(lootType);
            if (_audioSource) {
                _audioSource.Play();
                Destroy(GetComponent<MeshRenderer>());
                Destroy(GetComponent<MeshFilter>());
                Timer timer = new Timer();
                timer.OnEnd += DestroyCollectable;
                timer.Run(this, _audioSource.clip.length);
            } else Destroy(gameObject);
        }

        private void DestroyCollectable() {
            Destroy(gameObject);
        }

        public void OnHoverStart(MouseInteraction mouse) {
        }

        public void OnHoverEnd(MouseInteraction mouse) {
        }
    }
}
