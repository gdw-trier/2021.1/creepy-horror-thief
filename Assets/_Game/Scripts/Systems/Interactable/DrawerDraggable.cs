﻿using System;
using UnityEngine;

namespace _Game.Scripts.Systems.Interactable {
    public class DrawerDraggable : MonoBehaviour, IDraggable, IHoverable {
        [SerializeField] private Transform start, end, body;
        [SerializeField] private Vector3 offset;
        [SerializeField] private bool maintainMomentum = true;
        [SerializeField] private float damping = 5.0f;
        [SerializeField] private float strength = 1.0f;

        private Vector3 _Position;

        private float _PrevMapping;
        private float _LinearMapping;
        private float _InitialMappingOffset;

        private Vector2 _StartPoint, _EndPoint;

        private int _SampleCount = 0;
        private float _MappingChangeRate;
        private float[] _MappingChangeSamples;
        private float[] MappingChangeSamples => _MappingChangeSamples ??= new float[_SAMPLES];

        private Camera _Camera;
        private Camera Camera => _Camera ? _Camera : (_Camera = Camera.main);

        private Rigidbody _Rigidbody;

        private Rigidbody Rigidbody =>
            _Rigidbody ? _Rigidbody : (_Rigidbody = GetComponent<Rigidbody>());

        private const int _SAMPLES = 5;

        public bool IsValid {
            get {
                if (start && end && body)
                    return true;
                throw new NullReferenceException(
                    $"[Drawer]: {this.name}: Start, End or body is null");
            }
        }

        private void Awake() {
            body.localPosition = start.localPosition;
        }

        private void Update() {
            if (!IsValid || !maintainMomentum || !(Math.Abs(_LinearMapping) > 0.001f)) return;

            _MappingChangeRate = Mathf.Lerp(_MappingChangeRate, 0.0f, damping * Time.deltaTime);
            _LinearMapping = Mathf.Clamp01(_LinearMapping + _MappingChangeRate * Time.deltaTime);

            var pos =
                Vector3.Lerp(start.position, end.position, _LinearMapping);
            Rigidbody.MovePosition(pos);
        }

        public void DragStart() {
            if (!IsValid) return;

            _Position = Vector3.zero;

            _InitialMappingOffset = _LinearMapping - CalculateLinearMapping(_Position);
            _SampleCount = 0;
            _MappingChangeRate = 0.0f;

            _StartPoint = Camera.WorldToScreenPoint(start.position);
            _EndPoint = Camera.WorldToScreenPoint(end.position);
        }

        public void DragUpdate(Vector2 delta) {
            if (!IsValid) return;

            UpdateLinearMapping(delta);

            if (!maintainMomentum) {
                Rigidbody.MovePosition(Vector3.Lerp(start.position, end.position, _LinearMapping));
            }
        }

        public void DragEnd() {
            if (!IsValid) return;

            CalculateMappingChangeRate();
        }
        
        public void OnHoverStart(MouseInteraction mouse) {
        }

        public void OnHoverEnd(MouseInteraction mouse) {
        }

        private void CalculateDragPosition(Vector2 delta) {
            var dir = (_EndPoint - _StartPoint).normalized;
            _Position += (end.position - start.position).normalized *
                         (Vector2.Dot(dir, delta) * strength * Time.deltaTime);
        }

        private void UpdateLinearMapping(Vector3 delta) {
            CalculateDragPosition(delta);

            _PrevMapping = _LinearMapping;
            _LinearMapping =
                Mathf.Clamp01(_InitialMappingOffset + CalculateLinearMapping(_Position));

            MappingChangeSamples[_SampleCount % MappingChangeSamples.Length] =
                (1.0f / Time.deltaTime) * (_LinearMapping - _PrevMapping);
            _SampleCount++;
        }

        private float CalculateLinearMapping(Vector3 position) {
            Vector3 direction = end.position - start.position;
            float length = direction.magnitude;
            direction.Normalize();

            Vector3 displacement = position - start.position;

            return Vector3.Dot(displacement, direction) / length;
        }

        private void CalculateMappingChangeRate() {
            _MappingChangeRate = 0.0f;
            int mappingSamplesCount = Mathf.Min(_SampleCount, MappingChangeSamples.Length);
            if (mappingSamplesCount != 0) {
                for (int i = 0; i < mappingSamplesCount; ++i) {
                    _MappingChangeRate += MappingChangeSamples[i];
                }

                _MappingChangeRate /= mappingSamplesCount;
            }
        }

        
    }
}