﻿using System.Collections.Generic;
using _Game.Scripts.Utils;
using UnityEngine;

namespace _Game.Scripts.Systems.Sound {
    public class GlobalAudioSource : MonoSingleton<GlobalAudioSource> {
        private SoundSource _AmbientSource;

        public SoundSource AmbientSource =>
            _AmbientSource ??= new SoundSource(GetComponents<AudioSource>()[0], this);

        private SoundSource _OverlaySource;

        public SoundSource OverlaySource =>
            _OverlaySource ??= new SoundSource(GetComponents<AudioSource>()[1], this);
    }

    public class SoundSource {
        public AudioSource Source { get; private set; }
        private GlobalAudioSource _Global;

        public SoundSource(AudioSource source, GlobalAudioSource global) {
            Source = source;
            _Global = global;
        }

        private Queue<AudioClip> _Schedule;
        private Queue<AudioClip> Schedule => _Schedule ??= new Queue<AudioClip>();

        private AudioClip _Current;
        private Timer _Timer = new Timer();

        private void Run() {
            if (_Timer.Running) return;
            _Timer.OnEnd = null;
            if (Schedule.Count <= 0) return;
            var clip = Schedule.Dequeue();
            Source.clip = clip;
            Source.Play();
            //Source.PlayOneShot(clip, 1f);
            _Timer.Run(_Global, clip.length);
            _Timer.OnEnd += Run;
        }

        public void ScheduleClip(AudioClip clip) {
            Schedule.Enqueue(clip);
            if (!Source.isPlaying)
                Run();
        }
    }
}