﻿using _Game.Scripts.Utils;
using _Game.Scripts.Utils.Editor;
using UnityEngine;

namespace _Game.Scripts.Systems.Sound {
    [CreateAssetMenu(fileName = "Preset_Physics_Audio_Source",
        menuName = "Avaritia/Physics Audio Source Preset", order = 0)]
    public class PhysicsAudioPreset : ScriptableObject {
        public Vector2 magnitude = new Vector2(0.5f, 5f);
        [MinMaxSlider(0f, 1f), SerializeField] 
        private Vector2 volume = new Vector2(0f, 1f);
        [MinMaxSlider(-3f, 3f), SerializeField]
        private Vector2 pitch;

        [SerializeField] private AudioClip[] clips;
        public AudioClip[] Clips => clips;

        public AudioClip RandomClip() {
            if (clips.Length <= 0) return null;
            return clips[Random.Range(0, clips.Length)];
        }

        public float Volume(Vector3 velocity) {
            return velocity.magnitude.Remap(magnitude.x, magnitude.y, volume.x, volume.y);
        }

        public float Pitch(Vector3 velocity) {
            return velocity.magnitude.Remap(magnitude.x, magnitude.y, pitch.x, pitch.y);
        }
    }
}