﻿using System;
using UnityEngine;

namespace _Game.Scripts.Systems.Sound {
    [RequireComponent(typeof(AudioSource), typeof(Rigidbody))]
    public class PhysicsAudioSource : MonoBehaviour {
        public enum VelocityType {
            Linear,
            Angular,
        }

        [SerializeField] private VelocityType velocityType;
        [SerializeField] private PhysicsAudioPreset preset;

        private PhysicsAudioPreset Preset {
            get {
                if (preset) return preset;

                Debug.Log(
                    $"[PhysicsAudioSource]: Preset reference is missing, new instance will be created!",
                    this.gameObject);
                preset = ScriptableObject.CreateInstance<PhysicsAudioPreset>();
                return preset;
            }
        }

        private AudioSource _Source;
        public AudioSource Source => _Source ? _Source : (_Source = GetComponent<AudioSource>());

        private Rigidbody _Rigidbody;

        public Rigidbody Rigidbody =>
            _Rigidbody ? _Rigidbody : (_Rigidbody = GetComponent<Rigidbody>());


        private Vector3 _Velocity = Vector3.zero;

        private void FixedUpdate() {
            switch (velocityType) {
                case VelocityType.Linear:
                    Velocity = Rigidbody.velocity;
                    break;
                case VelocityType.Angular:
                    Velocity = Rigidbody.angularVelocity;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            if (Velocity.magnitude > Preset.magnitude.x) {
                Play();
            }
        }

        public Vector3 Velocity {
            get => _Velocity;
            set {
                _Velocity = value;
                if (_Velocity.magnitude < Preset.magnitude.x) {
                    Source.Stop();
                    return;
                }

                Source.volume = Preset.Volume(_Velocity);
                Source.pitch = Preset.Pitch(_Velocity);
            }
        }

        public void Play() {
            if (!Preset || Source.isPlaying || _Velocity.magnitude < Preset.magnitude.x) return;
            var clip = Preset.RandomClip();

            Source.clip = clip;
            Source.Play();
        }
    }
}