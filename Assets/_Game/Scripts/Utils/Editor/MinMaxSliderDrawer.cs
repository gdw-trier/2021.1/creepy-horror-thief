﻿using UnityEditor;
using UnityEngine;

namespace _Game.Scripts.Utils.Editor {
    [CustomPropertyDrawer(typeof(MinMaxSliderAttribute))]
    public class MinMaxSliderDrawer : PropertyDrawer {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var att = (MinMaxSliderAttribute) attribute;
            var propertyType = property.propertyType;

            label.tooltip = $"{att.min.ToString("F1")} to {att.max.ToString("F1")}";
            Rect control = EditorGUI.PrefixLabel(position, label);
            EditorGUI.indentLevel = 0;

            Rect[] splittedRect = SplitRect(control, 3);

            EditorGUI.BeginChangeCheck();
            Vector2 vector;
            switch (propertyType) {
                case SerializedPropertyType.Vector2:
                    vector = property.vector2Value;
                    MinMaxSlider(splittedRect, ref vector, att);
                    if (EditorGUI.EndChangeCheck()) {
                        property.vector2Value =
                            new Vector2((vector.x > vector.y ? vector.y : vector.x), vector.y);
                    }

                    break;
                case SerializedPropertyType.Vector2Int:
                    vector = property.vector2IntValue;
                    MinMaxSlider(splittedRect, ref vector, att);
                    if (EditorGUI.EndChangeCheck()) {
                        property.vector2IntValue = new Vector2Int(
                            Mathf.FloorToInt(vector.x > vector.y ? vector.y : vector.x),
                            Mathf.FloorToInt(vector.y));
                    }

                    break;
                default:
                    base.OnGUI(position, property, label);
                    return;
            }
        }

        private void MinMaxSlider(Rect[] positions, ref Vector2 vector, MinMaxSliderAttribute att) {
            float min = vector.x;
            float max = vector.y;

            min = EditorGUI.FloatField(positions[0], min);
            max = EditorGUI.FloatField(positions[2], max);

            EditorGUI.MinMaxSlider(positions[1], ref min, ref max, att.min, att.max);

            min = Mathf.Clamp(min, att.min, att.max);
            max = Mathf.Clamp(max, att.min, att.max);

            vector.x = min;
            vector.y = max;
        }

        private Rect[] SplitRect(Rect toSplit, int n) {
            Rect[] rects = new Rect[n];

            for (int i = 0; i < n; i++) {
                rects[i] = new Rect(toSplit.position.x + (i * toSplit.width / n),
                    toSplit.position.y, toSplit.width / n, toSplit.height);
            }

            int padding = (int) rects[0].width - 50;
            int space = 5;

            rects[0].width -= padding + space;
            rects[2].width -= padding + space;

            rects[1].x -= padding;
            rects[1].width += padding * 2;

            rects[2].x += padding + space;


            return rects;
        }
    }
}