﻿using System;
using UnityEngine;

namespace _Game.Scripts.Utils {
    public static class Maths {
        public enum Axis {
            Normal,
            Tangent,
            Bitangent,
        }

        public static void CoordSystemFromNormal(ref Vector3 normal, ref Vector3 tangent,
            ref Vector3 bitangent) {
            var tmp = new Vector3(normal.x, -normal.z, normal.y);
            if (Math.Abs(Math.Abs(Vector3.Dot(normal, tmp)) - 1) < 0.000001f) {
                tmp = new Vector3(-normal.y, normal.x, normal.z);
            }

            tangent = Vector3.Cross(normal, tmp);
            bitangent = Vector3.Cross(normal, tangent);
        }

        public static float Remap(this float value, float fromStart, float fromEnd, float toStart,
            float toEnd) {
            return toStart + (toEnd - toStart) * ((value - fromStart) / (fromEnd - fromStart));
        }
    }
}