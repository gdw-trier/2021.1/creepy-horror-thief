﻿using UnityEngine;

namespace _Game.Scripts.Utils {
    public class LookAt : MonoBehaviour {
        public Transform target;
        public float smoothness = 1f;
        public Vector3 offset;
        public EventTrigger trigger;

        private void OnEnable() {
            if (trigger) {
                trigger.OnEnter += Enter;
                trigger.OnExit += Exit;
            }
        }

        private void OnDisable() {
            if (trigger) {
                trigger.OnEnter -= Enter;
                trigger.OnExit -= Exit;
            }
        }

        private void Update() {
            if (!target) return;
            
            var targetRotation = Quaternion.LookRotation((target.position + offset) - transform.position, Vector3.up);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, smoothness);
        }

        private void Enter(Collider other) {
            SetTarget(other.transform);
        }

        private void Exit(Collider other) {
            SetTarget(null);
        }

        public void SetTarget(Transform target) {
            this.target = target;
        }
    }
}