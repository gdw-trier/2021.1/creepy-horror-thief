﻿using System;
using System.Collections;
using UnityEngine;

namespace _Game.Scripts.Utils {
    public class Timer {
        public bool Running { get; private set; }
        public Action OnStart, OnEnd;
        public Action<float> OnUpdate;
        
        private Coroutine _Current;

        public void Run(MonoBehaviour behaviour, float duration, Action<float> func = null) {
            if (Running) return;
            _Current = behaviour.StartCoroutine(Update(duration, func));
        }

        public void Stop(MonoBehaviour behaviour) {
            if (!Running) return;
            behaviour.StopCoroutine(_Current);
        }
        
        private IEnumerator Update(float duration, Action<float> func = null) {
            if (Running) yield break;
            Running = true;
            OnStart?.Invoke();
            var time = 0f;
            while (time <= 1.0f) {
                OnUpdate?.Invoke(time);
                func?.Invoke(time);
                time = time + Time.deltaTime / duration;
                yield return null;
            }

            Running = false;
            OnEnd?.Invoke();
        }
    }
}