﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace _Game.Scripts.Utils {
    [RequireComponent(typeof(Collider))]
    public class EventTrigger : MonoBehaviour {
        public UnityEvent OnEnterUnity, OnExitUnity;
        public event Action<Collider> OnEnter, OnExit;

        public void OnTriggerEnter(Collider other) {
            OnEnter?.Invoke(other);
            OnEnterUnity?.Invoke();
        }

        public void OnTriggerExit(Collider other) {
            OnExit?.Invoke(other);
            OnExitUnity?.Invoke();
        }
    }
}