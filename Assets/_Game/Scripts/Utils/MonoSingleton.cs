using UnityEngine;

namespace _Game.Scripts.Utils {
    public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T> {
        private static string _singletonObjName = "Singleton_Manager";
        private static GameObject _singletonObj;
        
        private static T _instance;

        public static T Instance {
            get {
                if (_instance) return _instance;

                var instances = FindObjectsOfType<T>();
                if (instances.Length > 0) {
                    _instance = instances[0];
                    for (int i = 1; i < instances.Length; i++) {
                        Destroy(instances[i]);
                    }

                    return _instance;
                }

                _singletonObj = GameObject.Find(_singletonObjName);
                if (_singletonObj != null) {
                    _instance = _singletonObj.AddComponent<T>();
                    return _instance;
                }

                _singletonObj = new GameObject(_singletonObjName);
                _instance = _singletonObj.AddComponent<T>();
                return _instance;
            }
        }
    }

    public class Singleton<T> where T : Singleton<T>, new() {
        private static T _instance;

        public static T Instance {
            get {
                if (_instance != null) return _instance;
                
                _instance = new T();
                return _instance;
            }
        }
    }
}