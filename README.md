In Avaritia schlüpfst du in die Haut einen Diebes, der auf der Suche nach großem Reichtum in ein verwunschenes, altes Haus eingebrochen ist. Dabei musst du versuchen, möglichst viele wertvolle Gegenstände zu stehlen, die überall im Haus versteckt sind. Und wo war eigentlich der Ausgang? Auf der Suche schleichst du dich durch das Haus… doch Vorsicht, es wird bewacht! Die Wachen zögern nicht, ihren Herrn zu rufen, falls sie dich entdecken… und mit ihm ist nicht zu spaßen. Entscheide selbst, ob du mit deiner Beute fliehst, wenn du einen Ausgang gefunden hast, oder ob du vielleicht doch noch in die nächste Etage weiter schleichst, in der noch kostbarere Gegenstände auf dich warten…

Licence information:
Sound effects obtained from https://www.zapsplat.com
